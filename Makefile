# ######### #
# VARIABLES #
# ######### #

# Load global environment variables from .env
include .env
export $(shell sed 's/=.*//' .env)

# ####### #
# TARGETS #
# ####### #

.PHONY: build
build:
	docker-compose -f infra/dev/docker-compose.dev.yaml build

.PHONY: up
up:
	docker-compose -p $(FORMATTED_PROJECT_NAME) -f infra/dev/docker-compose.stack.yaml up -d

.PHONY: down
down:
	docker-compose -p $(FORMATTED_PROJECT_NAME) -f infra/dev/docker-compose.stack.yaml stop

.PHONY: run
run:
	$(DOCKER_EXEC) sh

.PHONY: composer
composer:
	$(COMPOSER_COMPOSE_EXEC) run --rm composer

.PHONY: console
console:
	$(DOCKER_EXEC) ./bin/console $(COMMAND)

.PHONY: unit
unit:
	$(MAKE) .inner-tests TEST_SUITE=unit

.PHONY: functional
functional:
	$(MAKE) .inner-tests TEST_SUITE=functional

.PHONY: tests
tests: unit functional

.PHONY: coverage
coverage:
ifdef FILTER
	$(DOCKER_EXEC) ./vendor/bin/phpunit --config=phpunit-coverage.xml --testsuite $(TEST_SUITE) --filter "$(FILTER)"
else
	$(DOCKER_EXEC) ./vendor/bin/phpunit --config=phpunit-coverage.xml --testsuite $(TEST_SUITE)
endif
	@echo
	@echo 'Test documentation available at: ' ${PWD}/var/tests/coverage/index.html

.PHONY: .inner-tests
.inner-tests:
ifdef FILTER
	$(DOCKER_EXEC) ./vendor/bin/phpunit --testsuite $(TEST_SUITE) --filter "$(FILTER)" $(TARGET)
else
	$(DOCKER_EXEC) ./vendor/bin/phpunit --testsuite $(TEST_SUITE) $(TARGET)
endif
	@echo
	@echo 'Test documentation available at: ' ${PWD}/var/tests/documentation/testdox.html

# ######### #
# INTERNALS #
# ######### #
# Some logic to get a unique project identifier - for docker-compose jobs for example:
# - to avoid container name collision
# - but to keep meaningful name
# Example: your-project-name_run-tests_0b23a
export RANDOM_SLUG = $(shell LC_ALL=C tr -dc 'a-f0-9' </dev/urandom | head -c 5 ; echo)
export FORMATTED_CURRENT_TARGET = $(shell echo $(MAKECMDGOALS) | tr '[ ]' -)
export PROJECT_ID = $(FORMATTED_PROJECT_NAME)_$(FORMATTED_CURRENT_TARGET)_$(RANDOM_SLUG)

# Docker partial commands
export COMPOSER_COMPOSE_EXEC = docker-compose -f infra/dev/docker-compose.composer.yaml
export DOCKER_EXEC = docker-compose -f infra/dev/docker-compose.dev.yaml \
	run --rm --name=${PROJECT_ID} php

# PHPUnit
export TEST_SUITE := all