<?php

use FHuitelec\MealGenerator\Domain\Clock;
use FHuitelec\MealGenerator\Domain\Dish\DishProvider;
use FHuitelec\MealGenerator\Domain\Meal\WeekMealStore;
use FHuitelec\MealGenerator\Infrastructure\Provider\Deserializer\DishDeserializer;
use FHuitelec\MealGenerator\Infrastructure\Provider\Deserializer\IngredientDeserializer;
use FHuitelec\MealGenerator\Infrastructure\Provider\InMemoryDishProvider;
use FHuitelec\MealGenerator\Infrastructure\Store\WeekMealsFsStore;
use FHuitelec\MealGenerator\Infrastructure\SystemClock;

$container->autowire(DishDeserializer::class);
$container->autowire(IngredientDeserializer::class)
    ->setArgument('$ingredients', '%ingredients%');

$container->autowire(InMemoryDishProvider::class)
    ->setArgument('$dishes', '%dishes%');

/** @noinspection PhpUnhandledExceptionInspection */
$container->setAlias(DishProvider::class, InMemoryDishProvider::class);
/** @noinspection PhpUnhandledExceptionInspection */
$container->setAlias(WeekMealStore::class, WeekMealsFsStore::class);
/** @noinspection PhpUnhandledExceptionInspection */
$container->setAlias(Clock::class, SystemClock::class);