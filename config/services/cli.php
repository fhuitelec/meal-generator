<?php

use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\Listener\MealReplacedListener;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\Listener\RefreshListener;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\Listener\WeekMealsGeneratedListener;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\MealReplaced;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\NewQuestionAsked;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\WeekMealsGenerated;

$dashboardEventRefresh = [
    WeekMealsGenerated::NAME,
    MealReplaced::NAME,
    NewQuestionAsked::NAME
];

$container->autowire(WeekMealsGeneratedListener::class)
    ->addTag('kernel.event_listener', [
        'event' => WeekMealsGenerated::NAME,
        'method' => 'onWeekMealsGenerated'
    ]);

$container->autowire(MealReplacedListener::class)
    ->addTag('kernel.event_listener', [
        'event' => MealReplaced::NAME,
        'method' => 'onMealReplaced'
    ]);

foreach ($dashboardEventRefresh as $eventName) {
    $container->autowire(RefreshListener::class)
        ->addTag('kernel.event_listener', [
            'event' => $eventName,
            'method' => 'refresh'
        ]);
}