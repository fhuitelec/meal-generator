ARG PHP_VERSION
FROM php:${PHP_VERSION}-cli-alpine

RUN apk --update add --no-cache --virtual .build-deps autoconf alpine-sdk openssl-dev \
    && pecl install xdebug-2.5.0 \
    && pecl install mongodb-1.3.4 \
    && docker-php-ext-enable xdebug mongodb \
    && apk del .build-deps

RUN apk --update add ncurses

RUN touch /usr/local/etc/php/php.ini
RUN echo 'xdebug.remote_enable=1'           >> /usr/local/etc/php/php.ini
RUN echo 'date.timezone = "Europe/Paris"'   >> /usr/local/etc/php/php.ini