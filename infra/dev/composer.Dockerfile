FROM php:7.1-alpine

RUN apk --update add git

RUN wget https://getcomposer.org/composer.phar -P /tmp
RUN chmod +x /tmp/composer.phar
RUN mv /tmp/composer.phar /usr/local/bin/composer

RUN composer global require hirak/prestissimo
RUN composer global require symfony/flex