<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Meal;

use FHuitelec\MealGenerator\Domain\Dish\Collection\Ingredients;
use PHPUnit\Framework\TestCase;
use function FHuitelec\MealGenerator\Tests\Unit\System\MealGeneration\aMeal;

class MealTest extends TestCase
{
    /**
     * @test
     *
     * @throws \PHPUnit\Framework\Exception
     */
    public function it returns its ingredients()
    {
        // Arrange
        $meal = aMeal()->build();

        // Act & Assert
        $this->assertInstanceOf(
            Ingredients::class,
            $meal->getDishIngredients()
        );
    }
}