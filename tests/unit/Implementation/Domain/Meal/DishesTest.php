<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Meal;

use FHuitelec\MealGenerator\Domain\Dish\Collection\Dishes;
use FHuitelec\MealGenerator\Tests\Unit\Helper\Builder\DishBuilder;
use PHPUnit\Framework\TestCase;

class DishesTest extends TestCase
{
    /**
     * @test
     *
     * @throws \PHPUnit\Framework\Exception
     */
    public function it returns dish by name()
    {
        // Arrange
        $dishes = Dishes::fromArray([
            (new DishBuilder())->withName('Pizza')->build(),
            (new DishBuilder())->withName('Pâtes carbos')->build(),
        ]);

        // Act
        $dishNames = $dishes->toDishNameArray();

        // Assert
        $this->assertContains('Pizza', $dishNames);
        $this->assertContains('Pâtes carbos', $dishNames);
    }

    /**
     * @test
     */
    public function it gets a dish by its name()
    {
        $expectedDish = (new DishBuilder())->withName('Pizza')->build();
        // Arrange
        $dishes = Dishes::fromArray([
            $expectedDish,
            (new DishBuilder())->withName('Pâtes carbos')->build(),
        ]);

        // Act
        $dish = $dishes->getByName('Pizza');

        // Assert
        $this->assertEquals($expectedDish, $dish);
    }
}