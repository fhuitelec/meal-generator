<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Meal\Collection;

use FHuitelec\MealGenerator\Domain\Meal\Day;
use FHuitelec\MealGenerator\Domain\Meal\Identity\WeekMealsId;
use function FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Command\aWeekWorthOfMeals;
use PHPUnit\Framework\TestCase;

class WeekMealsTest extends TestCase
{
    /**
     * @test
     */
    public function it returns meals from a specific day of the week()
    {
        // Arrange
        $sut = aWeekWorthOfMeals()->build();

        // Act
        $dayMeals = $sut->getByDay(Day::THURSDAY);

        // Assert
        $this->assertEquals(Day::THURSDAY, $dayMeals->getFormattedDay());
    }

    /**
     * @test
     */
    public function it ignores when requesting meals from an invalid day of the week()
    {
        // Arrange
        $sut = aWeekWorthOfMeals()->build();

        // Act
        $dayMeals = $sut->getByDay('Never');

        // Assert
        $this->assertNull($dayMeals);
    }

    /** @test */
    public function it returns its ID()
    {
        // Arrange
        $expectedId = WeekMealsId::random();
        $sut = aWeekWorthOfMeals()->withId($expectedId)->build();

        // Act & Assert
        self::assertEquals($expectedId, $sut->getId());
    }

    /** @test */
    public function it returns its creation date()
    {
        // Arrange
        $expectedCreationDate = new \DateTimeImmutable('2017-02-03');
        $sut = aWeekWorthOfMeals()
            ->withACreationDate($expectedCreationDate)
            ->build();

        // Act & Assert
        $this->assertEquals($expectedCreationDate, $sut->getCreatedAt());
    }
}