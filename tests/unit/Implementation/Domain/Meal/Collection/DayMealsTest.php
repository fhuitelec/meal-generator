<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Meal\Collection;

use FHuitelec\MealGenerator\Domain\Meal\Collection\DayMeals;
use FHuitelec\MealGenerator\Domain\Meal\Day;
use function FHuitelec\MealGenerator\Tests\Unit\System\MealGeneration\aDish;
use function FHuitelec\MealGenerator\Tests\Unit\System\MealGeneration\aMeal;
use PHPUnit\Framework\TestCase;

class DayMealsTest extends TestCase
{
    /**
     * @test
     *
     * @throws \PHPUnit\Framework\Exception
     */
    public function it exposes meal s names()
    {
        // Arrange
        $meals = new DayMeals(
            new Day(new \DateTimeImmutable()),
            aMeal()
                ->withDish(aDish()->withName('Pizza')->build())
                ->build(),
            aMeal()
                ->withDish(aDish()->withName('Spaghettis')->build())
                ->build()
        );

        // Act
        $mealNames = $meals->toMealNameArray();

        // Assert
        $this->assertCount(2, $mealNames);
        $this->assertEquals('Pizza', $mealNames[0]);
        $this->assertEquals('Spaghettis', $mealNames[1]);
    }

    /**
     * @test
     *
     * @throws \PHPUnit\Framework\AssertionFailedError
     */
    public function it tells if it is a specific day of the week()
    {
        // Arrange
        $sut = new DayMeals(
            new Day(new \DateTimeImmutable('next monday')),
            aMeal()->build(),
            aMeal()->build()
        );

        $this->assertTrue($sut->isDay(Day::MONDAY));
    }
}