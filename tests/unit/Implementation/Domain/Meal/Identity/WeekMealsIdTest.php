<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Meal\Identity;

use FHuitelec\MealGenerator\Domain\Meal\Identity\WeekMealsId;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class WeekMealsIdTest extends TestCase
{
    /** @test */
    public function it returns a string()
    {
        // Arrange
        $expectedId = '1d9b7f91-eee0-4547-b7bf-f9613a2b43b0';
        $id = new WeekMealsId(
            Uuid::fromString($expectedId)
        );

        // Act & Assert
        $this->assertEquals($expectedId, (string)$id);
    }
}