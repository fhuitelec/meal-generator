<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Ingredient\Identity;

use FHuitelec\MealGenerator\Domain\Dish\Identity\IngredientId;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class IngredientIdTest extends TestCase
{
    /**
     * @test
     */
    public function it can be casted to a string()
    {
        // Arrange
        $expectedStringId = 'f00ddb5c-602c-4939-b5fd-af55f5f5fd9f';
        $ingredientId = new IngredientId(Uuid::fromString($expectedStringId));

        // Act & Assert
        $this->assertEquals(
            $expectedStringId,
            (string)$ingredientId
        );
    }
}