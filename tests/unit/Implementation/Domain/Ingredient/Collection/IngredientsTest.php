<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Ingredient\Collection;

use FHuitelec\MealGenerator\Domain\Dish\Collection\Ingredients;
use PHPUnit\Framework\TestCase;
use function FHuitelec\MealGenerator\Tests\Unit\System\Ingredient\anIngredient;

class IngredientsTest extends TestCase
{
    /**
     * @test
     *
     * @throws \PHPUnit\Framework\Exception
     */
    public function ingredients can add other ingredients()
    {
        // Arrange
        $ingredient1 = Ingredients::fromArray([
            anIngredient()->build(),
            anIngredient()->build()
        ]);
        $ingredient2 = Ingredients::fromArray([
            anIngredient()->build(),
            anIngredient()->build(),
            anIngredient()->build()
        ]);

        // Act
        $addedIngredients = $ingredient1->add($ingredient2);

        // Assert
        $this->assertCount(5, $addedIngredients->toArray());
    }
}