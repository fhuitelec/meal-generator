<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Ingredient;

use FHuitelec\MealGenerator\Domain\Dish\Identity\IngredientId;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use function FHuitelec\MealGenerator\Tests\Unit\System\Ingredient\anIngredient;

class IngredientTest extends TestCase
{
    /**
     * @test
     */
    public function it returns its ID()
    {
        // Arrange
        $expectedId = Uuid::uuid4();
        $ingredient = anIngredient()->withId($expectedId)->build();

        // Act && Assert
        $this->assertEquals(
            new IngredientId($expectedId),
            $ingredient->getId()
        );
    }

    /**
     * @test
     */
    public function it returns its name()
    {
        // Arrange
        $ingredient = anIngredient()->withName('Tomate')->build();

        // Act & Assert
        $this->assertEquals('Tomate', $ingredient->getName());
    }
}