<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Command;

use FHuitelec\MealGenerator\Domain\Command\SaveWeekMealsCommand;
use FHuitelec\MealGenerator\Tests\Unit\Helper\Builder\Collection\WeekMealsBuilder;
use PHPUnit\Framework\TestCase;

class SaveWeekMealsCommandTest extends TestCase
{
    /**
     * @test
     */
    public function it accepts a weeks worth of meals()
    {
        // Arrange
        $weekMeals = aWeekWorthOfMeals()->build();
        $sut = new SaveWeekMealsCommand($weekMeals);

        // Act & Assert
        $this->assertEquals($weekMeals, $sut->getWeekMeals());
    }
}

/** @return WeekMealsBuilder */
function aWeekWorthOfMeals(): WeekMealsBuilder
{
    return new WeekMealsBuilder();
}
