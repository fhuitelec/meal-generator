<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Command\Handler;

use FHuitelec\MealGenerator\Domain\Command\Handler\SaveWeekMealsHandler;
use FHuitelec\MealGenerator\Domain\Command\SaveWeekMealsCommand;
use FHuitelec\MealGenerator\Domain\Meal\WeekMealStore;
use function FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Command\aWeekWorthOfMeals;
use Mockery\Mock;
use PHPUnit\Framework\TestCase;

class SaveWeekMealsHandlerTest extends TestCase
{
    /** @var WeekMealStore|Mock */
    private $store;
    /** @var SaveWeekMealsHandler */
    private $sut;

    protected function setUp()
    {
        $this->store = \Mockery::spy(WeekMealStore::class);
        $this->sut = new SaveWeekMealsHandler($this->store);
    }

    protected function tearDown()
    {
        $this->addToAssertionCount(\Mockery::getContainer()->mockery_getExpectationCount());
        \Mockery::close();
    }

    /**
     * @test
     */
    public function it stores a weeks worth of meals()
    {
        // Arrange
        $expectedWeekMeals = aWeekWorthOfMeals()->build();
        $command = new SaveWeekMealsCommand($expectedWeekMeals);

        // Act
        $this->sut->handle($command);

        // Assert
        $this->store->shouldHaveReceived('store')
            ->with($expectedWeekMeals)
            ->once();
    }
}