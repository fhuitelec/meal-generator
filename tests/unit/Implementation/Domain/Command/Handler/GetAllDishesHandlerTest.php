<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Command\Handler;

use FHuitelec\MealGenerator\Domain\Command\GetAllDishesQuery;
use FHuitelec\MealGenerator\Domain\Command\Handler\GetAllDishesHandler;
use FHuitelec\MealGenerator\Domain\Dish\Collection\Dishes;
use FHuitelec\MealGenerator\Domain\Dish\DishProvider;
use FHuitelec\MealGenerator\Tests\Unit\Helper\Builder\DishBuilder;
use Mockery\Mock;
use PHPUnit\Framework\TestCase;

class GetAllDishesHandlerTest extends TestCase
{
    /** @var DishProvider|Mock */
    private $dishProvider;
    /** @var GetAllDishesHandler */
    private $sut;

    protected function setUp()
    {
        $this->dishProvider = \Mockery::mock(DishProvider::class);
        $this->dishProvider->shouldReceive('findAllDishes')
            ->byDefault();
        $this->sut = new GetAllDishesHandler(
            $this->dishProvider
        );
    }

    /**
     * @test
     *
     * @throws \InvalidArgumentException
     */
    public function it returns all existing dishes()
    {
        // Arrange
        $expectedDishes = Dishes::fromArray([
            (new DishBuilder())->build(),
            (new DishBuilder())->build(),
            (new DishBuilder())->build(),
        ]);
        $this->dishProvider->shouldReceive('findAllDishes')
            ->andReturn($expectedDishes);

        // Act
        $dishes = $this->sut->handle(new GetAllDishesQuery());

        // Assert
        $this->assertEquals($expectedDishes, $dishes);
    }
}