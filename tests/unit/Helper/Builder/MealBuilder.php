<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Helper\Builder;

use FHuitelec\MealGenerator\Domain\Dish\Dish;
use FHuitelec\MealGenerator\Domain\Meal\Identity\MealId;
use FHuitelec\MealGenerator\Domain\Meal\Meal;
use Ramsey\Uuid\Uuid;

class MealBuilder
{
    /** @var MealId */
    private $id;
    /** @var Dish */
    private $dish;

    public function __construct()
    {
        $this->id = new MealId(Uuid::uuid4());
        $this->dish = (new DishBuilder())->build();
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function withId(string $id): self
    {
        $this->id = new MealId(Uuid::fromString($id));

        return $this;
    }

    /**
     * @param Dish $dish
     *
     * @return $this
     */
    public function withDish(Dish $dish): self
    {
        $this->dish = $dish;

        return $this;
    }

    /** @return Meal */
    public function build(): Meal
    {
        return new Meal(
            $this->id,
            $this->dish
        );
    }
}