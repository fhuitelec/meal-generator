<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Helper\Builder;

use Faker\Factory;
use FHuitelec\MealGenerator\Domain\Dish\Collection\Ingredients;
use FHuitelec\MealGenerator\Domain\Dish\Dish;
use FHuitelec\MealGenerator\Domain\Dish\Identity\DishId;
use Ramsey\Uuid\Uuid;
use function FHuitelec\MealGenerator\Tests\Unit\System\Ingredient\anIngredient;

class DishBuilder
{
    /** @var DishId */
    private $id;
    /** @var string */
    private $name;
    /** @var Ingredients */
    private $ingredients;
    /** @var bool */
    private $canBeGenerated;

    /**
     * MealBuilder constructor.
     */
    public function __construct()
    {
        $faker = Factory::create();

        $this->id = new DishId(Uuid::uuid4());
        $this->name = $faker->sentence($faker->numberBetween(1, 3));
        $this->ingredients = Ingredients::fromArray([]);
        $this->canBeGenerated = true;
    }

    /** @return $this */
    public function thatCannotBeGenerated(): self
    {
        $this->canBeGenerated = false;

        return $this;
    }

    /**
     * @param int $number
     *
     * @return $this
     */
    public function withIngredientsNumber(int $number): self
    {
        $ingredients = [];

        for ($i = 0; $i < $number; $i++) {
            $ingredients[] = anIngredient()->build();
        }

        $this->ingredients = Ingredients::fromArray($ingredients);

        return $this;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function withId(string $id): self
    {
        $this->id = new DishId(Uuid::fromString($id));

        return $this;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function withName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /** @return Dish */
    public function build(): Dish
    {
        return new Dish(
            $this->id,
            $this->name,
            $this->ingredients,
            $this->canBeGenerated
        );
    }
}