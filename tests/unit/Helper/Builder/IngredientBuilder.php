<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Helper\Builder;

use Faker\Factory;
use FHuitelec\MealGenerator\Domain\Dish\Identity\IngredientId;
use FHuitelec\MealGenerator\Domain\Dish\Ingredient;
use PhpUnitsOfMeasure\PhysicalQuantity\Mass;
use PhpUnitsOfMeasure\PhysicalQuantity\Quantity;
use PhpUnitsOfMeasure\PhysicalQuantityInterface;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class IngredientBuilder
{
    /** @var IngredientId */
    private $id;
    /** @var string */
    private $name;
    /** @var PhysicalQuantityInterface */
    private $quantifier;

    public function __construct()
    {
        $faker = Factory::create();

        $this->id = new IngredientId(Uuid::uuid4());
        $this->name = $faker->sentence($faker->numberBetween(1, 2));
        $this->quantifier = new Quantity(2, 'mol');
    }

    /**
     * @param UuidInterface $id
     *
     * @return $this
     */
    public function withId(UuidInterface $id): self
    {
        $this->id = new IngredientId($id);

        return $this;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function withName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param float $kilos
     *
     * @return $this
     */
    public function withMass(float $kilos): self
    {
        $this->quantifier = new Mass($kilos, 'kilograms');

        return $this;
    }

    /**
     * @param float $quantity
     *
     * @return $this
     */
    public function withQuantity(float $quantity): self
    {
        $this->quantifier = new Quantity($quantity, 'mol');

        return $this;
    }

    /** @return Ingredient */
    public function build(): Ingredient
    {
        return new Ingredient(
            $this->id,
            $this->name,
            $this->quantifier
        );
    }
}