<?php
namespace FHuitelec\MealGenerator\Tests\Unit\Helper\Builder\Collection;

use Faker\Factory;
use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Domain\Meal\Day;
use FHuitelec\MealGenerator\Domain\Meal\Identity\WeekMealsId;
use FHuitelec\MealGenerator\Domain\Meal\Meal;
use FHuitelec\MealGenerator\Domain\Meal\Week;
use FHuitelec\MealGenerator\Tests\Unit\Helper\Builder\MealBuilder;

class WeekMealsBuilder
{
    const MONDAY = 0;
    const TUESDAY = 2;
    const WEDNESDAY = 4;
    const THURSDAY = 6;
    const FRIDAY = 8;
    const SATURDAY = 10;
    const SUNDAY = 12;

    const LUNCH = 0;
    const DINNER = 1;

    /** @var WeekMealsId */
    private $id;
    /** @var array|Meal[] */
    private $meals;
    /** @var \DateTimeImmutable */
    private $creationDate;

    public function __construct()
    {
        $meals = [];

        for ($i = 0; $i < 14; $i++) {
            $meals[] = (new MealBuilder())->build();
        }

        $this->id = WeekMealsId::random();
        $this->meals = $meals;
        $this->creationDate = new \DateTimeImmutable();
    }

    /**
     * @param WeekMealsId $weekMealsId
     *
     * @return $this
     */
    public function withId(WeekMealsId $weekMealsId): self
    {
        $this->id = $weekMealsId;

        return $this;
    }
    /**
     * @param \DateTimeImmutable $creationDate
     *
     * @return $this
     */
    public function withACreationDate(\DateTimeImmutable $creationDate): self
    {
        $this->creationDate = $creationDate;

        return $this;
    }
    /**
     * @param Meal $meal
     * @param int  $day
     * @param int  $momentOfTheDay
     *
     * @return WeekMealsBuilder
     */
    public function withAMeal(Meal $meal, int $day = null, int $momentOfTheDay = null): self
    {
        $faker = Factory::create();

        if (null === $day) {
            $day = $faker->randomElement([
                self::MONDAY, self::TUESDAY, self::WEDNESDAY, self::THURSDAY,
                self::FRIDAY, self::SATURDAY, self::SUNDAY
            ]);
        }

        if (null === $momentOfTheDay) {
            $day = $faker->randomElement([self::LUNCH, self::DINNER]);
        }

        $this->meals[$day + $momentOfTheDay] = $meal;

        return $this;
    }

    /** @return WeekMeals */
    public function build(): WeekMeals
    {
        return WeekMeals::create(
            $this->id,
            Week::startingNextMonday(new \DateTimeImmutable()),
            $this->meals,
            $this->creationDate
        );
    }
}