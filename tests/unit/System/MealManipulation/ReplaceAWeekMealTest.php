<?php
namespace FHuitelec\MealGenerator\Tests\Unit\System\MealManipulation;

use FHuitelec\MealGenerator\Domain\Command\Handler\ReplaceAWeekMealHandler;
use FHuitelec\MealGenerator\Domain\Command\ReplaceAWeekMealCommand;
use FHuitelec\MealGenerator\Domain\Meal\Collection\DayMeals;
use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Domain\Meal\Day;
use FHuitelec\MealGenerator\Domain\Meal\WeekMealStore;
use FHuitelec\MealGenerator\Tests\Unit\Helper\Builder\Collection\WeekMealsBuilder;
use FHuitelec\MealGenerator\Tests\Unit\Helper\Builder\DishBuilder;
use FHuitelec\MealGenerator\Tests\Unit\Helper\Builder\MealBuilder;
use Mockery\Mock;
use PHPUnit\Framework\TestCase;

class ReplaceAWeekMealTest extends TestCase
{
    /** @var WeekMealStore|Mock */
    private $weekMealsStore;
    /** @var ReplaceAWeekMealHandler */
    private $sut;

    protected function setUp()
    {
        $this->weekMealsStore = \Mockery::spy(WeekMealStore::class);
        $this->weekMealsStore->shouldReceive('store')->byDefault();

        $this->sut = new ReplaceAWeekMealHandler(
            $this->weekMealsStore
        );
    }

    protected function tearDown()
    {
        $this->addToAssertionCount(
            \Mockery::getContainer()->mockery_getExpectationCount()
        );
    }

    /**
     * @test
     *
     * @throws \PHPUnit\Framework\Exception
     */
    public function it replaces a meal within a week worth of meals()
    {
        // Arrange
        $mealToReplace = (new MealBuilder())->build();
        $newDish = (new DishBuilder())->build();
        $command = new ReplaceAWeekMealCommand(
            $mealToReplace->getId(),
            $newDish,
            (new WeekMealsBuilder())->withAMeal($mealToReplace)->build()
        );

        // Act
        $weekMeals = $this->sut->handle($command);

        // Assert
        $dishIds = array_reduce($weekMeals->getMealsByDay(), function(array $mealIds, DayMeals $dayMeals) {
            $mealIds[] = (string)$dayMeals->getLunch()->getDishId();
            $mealIds[] = (string)$dayMeals->getDinner()->getDishId();

            return $mealIds;
        }, []);

        $this->assertContains((string)$newDish->getId(), $dishIds, 'The week worth of meals does not contain the new meal');
        $this->assertNotContains((string)$mealToReplace->getDishId(), $dishIds, 'The week worth of meals still contain the meal to replace');
    }

    /** @test */
    public function it does not affect the other meals()
    {
        // Arrange
        $mealToReplace = (new MealBuilder())->build();
        $newDish = (new DishBuilder())->build();
        $weekMeals = (new WeekMealsBuilder())->withAMeal(
                $mealToReplace,
                WeekMealsBuilder::FRIDAY,
                WeekMealsBuilder::LUNCH
        )->build();
        $command = new ReplaceAWeekMealCommand($mealToReplace->getId(), $newDish, $weekMeals);

        // Act
        $newWeekMeals = $this->sut->handle($command);

        // Assert
        $expectedMealsByDay = $weekMeals->getMealsByDay();
        $mealsByDay = $newWeekMeals->getMealsByDay();

        $daysToAssert = [
            Day::MONDAY, Day::TUESDAY, Day::WEDNESDAY, Day::THURSDAY,
            Day::FRIDAY, Day::SATURDAY, Day::SUNDAY,
        ];

        foreach ($daysToAssert as $day) {
            $this->assertEquals(
                (string)$expectedMealsByDay[$day]->getLunch()->getId(),
                (string)$mealsByDay[$day]->getLunch()->getId()
            );
            $this->assertEquals(
                (string)$expectedMealsByDay[$day]->getDinner()->getId(),
                (string)$mealsByDay[$day]->getDinner()->getId()
            );
        }
    }

    /** @test */
    public function it store the new week meal()
    {
        // Arrange
        $mealToReplace = (new MealBuilder())->build();
        $newDish = (new DishBuilder())->build();
        $weekMeals = (new WeekMealsBuilder())->withAMeal(
                $mealToReplace,
                WeekMealsBuilder::FRIDAY,
                WeekMealsBuilder::LUNCH
        )->build();
        $command = new ReplaceAWeekMealCommand($mealToReplace->getId(), $newDish, $weekMeals);

        // Act
        $newWeekMeals = $this->sut->handle($command);

        // Assert
        $this->weekMealsStore->shouldHaveReceived('store')
            ->with($newWeekMeals)
            ->once();
   }
}