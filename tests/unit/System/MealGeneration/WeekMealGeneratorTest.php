<?php

namespace FHuitelec\MealGenerator\Tests\Unit\System\MealGeneration;

use FHuitelec\MealGenerator\Domain\Command\GenerateWeekMealsCommand;
use FHuitelec\MealGenerator\Domain\Command\Handler\GenerateWeekMealsHandler;
use FHuitelec\MealGenerator\Domain\Dish\Exception\NotEnoughDishesFound;
use FHuitelec\MealGenerator\Domain\Meal\Collection\DayMeals;
use FHuitelec\MealGenerator\Domain\Meal\Collection\Meals;
use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Domain\Meal\MealGenerator;
use FHuitelec\MealGenerator\Infrastructure\SystemClock;
use function FHuitelec\MealGenerator\Tests\Unit\Implementation\Domain\Command\aWeekWorthOfMeals;
use Mockery\Mock;
use PHPUnit\Framework\TestCase;

class WeekMealGeneratorTest extends TestCase
{
    /** @var MealGenerator|Mock */
    private $mealGenerator;
    /** @var GenerateWeekMealsHandler */
    private $sut;

    protected function setUp()
    {
        $this->mealGenerator = \Mockery::spy(MealGenerator::class);

        $this->sut = new GenerateWeekMealsHandler($this->mealGenerator);
    }

    /**
     * @test
     *
     * @throws NotEnoughDishesFound|\InvalidArgumentException
     * @throws \PHPUnit\Framework\Exception
     */
    public function it generates 2 meals for each day of the week()
    {
        // Arrange
        $this->mealGenerator->shouldReceive('generateWeek')
            ->andReturn(aWeekWorthOfMeals()->build());

        // Act
        $weekMeals = $this->sut->handle(new GenerateWeekMealsCommand());

        // Assert
        $this->mealGenerator
            ->shouldHaveReceived('generateWeek');

        $this->assertInstanceOf(WeekMeals::class, $weekMeals);
    }
}
