<?php

namespace FHuitelec\MealGenerator\Tests\Unit\System\MealGeneration;

use FHuitelec\MealGenerator\Domain\Dish\Collection\Dishes;
use FHuitelec\MealGenerator\Domain\Dish\DishProvider;
use FHuitelec\MealGenerator\Domain\Dish\Exception\NotEnoughDishesFound;
use FHuitelec\MealGenerator\Domain\Meal\Collection\DayMeals;
use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Domain\Meal\Meal;
use FHuitelec\MealGenerator\Domain\Meal\MealGenerator;
use FHuitelec\MealGenerator\Infrastructure\SystemClock;
use FHuitelec\MealGenerator\Tests\Unit\Helper\Builder\DishBuilder;
use FHuitelec\MealGenerator\Tests\Unit\Helper\Builder\MealBuilder;
use Mockery\Mock;
use PHPUnit\Framework\Exception;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class MealGeneratorTest extends TestCase
{
    /** @var MealGenerator */
    private $sut;
    /** @var DishProvider|Mock */
    private $mealProvider;

    protected function setUp()
    {
        $this->mealProvider = \Mockery::mock(DishProvider::class);
        $this->mealProvider
            ->shouldReceive('findAllDishes')
            ->andReturn(randomDishes())
            ->byDefault();

        $this->sut = new MealGenerator(new SystemClock(), $this->mealProvider);
    }

    /**
     * @test
     *
     * @throws NotEnoughDishesFound|\InvalidArgumentException
     * @throws Exception
     */
    public function it generates a week worth of meals()
    {
        // Arrange
        $this->mealProvider->shouldReceive('findAllDishes')
            ->andReturn(randomDishes(14));

        // Act
        $meals = $this->sut->generateWeek();

        // Assert
        $this->assertInstanceOf(WeekMeals::class, $meals);
    }

    /**
     * @test
     *
     * @throws NotEnoughDishesFound|\InvalidArgumentException
     * @throws Exception
     */
    public function it cannot generate anything when not enough dishes have been found()
    {
        // Arrange
        $this->expectException(NotEnoughDishesFound::class);
        $this->mealProvider->shouldReceive('findAllDishes')
            ->andReturn(randomDishes(5));

        // Act
        $this->sut->generateWeek();
    }

    /**
     * @test
     *
     * @throws NotEnoughDishesFound|\InvalidArgumentException
     * @throws Exception
     */
    public function it generates unique meals()
    {
        // Arrange
        $this->mealProvider->shouldReceive('findAllDishes')
            ->andReturn(randomDishes(14));

        // Act
        $meals = $this->sut->generateWeek();

        // Assert
        $dishIds = array_reduce(
            $meals->getMealsByDay(),
            function (array $dishIds, DayMeals $dayMeals) {
                $dishIds[] = (string)$dayMeals->getLunch()->getDishId();
                $dishIds[] = (string)$dayMeals->getDinner()->getDishId();

                return $dishIds;
            }, []
        );

        $this->assertFalse($this->hasDuplicateEntries($dishIds));
    }

    /**
     * @param array $arrayToAssert
     *
     * @return bool
     */
    private function hasDuplicateEntries(array $arrayToAssert): bool
    {
        $arraySize = count($arrayToAssert);

        $withoutDuplicatesSize = count(
            array_unique(array_map('strtoupper', $arrayToAssert))
        );

        return $arraySize !== $withoutDuplicatesSize;
    }

    /**
     * @test
     *
     * @throws \InvalidArgumentException|NotEnoughDishesFound
     * @throws Exception
     */
    public function it does not include dishes that cannot be generated()
    {
        // Expectation
        self::expectException(NotEnoughDishesFound::class);

        // Arrange
        $dishes = randomDishes(13)->toArray();
        $dishes[] = aDish()
            ->thatCannotBeGenerated()
            ->build();

        $this->mealProvider
            ->shouldReceive('findAllDishes')
            ->andReturn(Dishes::fromArray($dishes));

        // Act
        $this->sut->generateWeek();
    }
}

/** @return DishBuilder */
function aDish(): DishBuilder
{
    return new DishBuilder();
}

/** @return MealBuilder */
function aMeal(): MealBuilder
{
    return new MealBuilder();
}

/** @return string */
function aRandomId(): string
{
    return Uuid::uuid4()->toString();
}

/**
 * @param int $count
 *
 * @return Meal[]
 */
function randomMeals(int $count = 5): array
{
    $meals = [];

    for($i = 0; $i < $count; $i++) {
        $meals[] = aMeal()->build();
    }

    return $meals;
}

/**
 * @param int $count
 *
 * @return Dishes
 */
function randomDishes(int $count = 5): Dishes
{
    $dishes = [];

    for($i = 0; $i < $count; $i++) {
        $dishes[] = aDish()->build();
    }

    return Dishes::fromArray($dishes);
}