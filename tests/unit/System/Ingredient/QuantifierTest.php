<?php
namespace FHuitelec\MealGenerator\Tests\Unit\System\Ingredient;

use FHuitelec\MealGenerator\Domain\Dish\Exception\IngredientCannotBeAggregated;
use FHuitelec\MealGenerator\Domain\Dish\Ingredient;
use FHuitelec\MealGenerator\Tests\Unit\Helper\Builder\IngredientBuilder;
use PHPUnit\Framework\Exception;
use PHPUnit\Framework\TestCase;
use PhpUnitsOfMeasure\PhysicalQuantity\Quantity;
use Ramsey\Uuid\Uuid;

class QuantifierTest extends TestCase
{
    /**
     * @test
     * @dataProvider canAggregateIngredients
     *
     * @param Ingredient $ingredient1
     * @param Ingredient $ingredient2
     * @param bool       $expectedCanAggregate
     */
    public function an ingredient indicates when it can be aggregated with another ingredient(
        Ingredient $ingredient1,
        Ingredient $ingredient2,
        bool $expectedCanAggregate
    ) {
        // Act
        $canAggregate = $ingredient1->canAggregate($ingredient2);

        // Assert
        $this->assertEquals($expectedCanAggregate, $canAggregate);
    }

    /**
     * @test
     *
     * @throws IngredientCannotBeAggregated
     * @throws \PHPUnit\Framework\Exception
     */
    public function an ingredient can aggregate another ingredient()
    {
        // Arrange
        $sameId = Uuid::uuid4();
        $ingredient1 = anIngredient()->withId($sameId)->withQuantity(2)->build();
        $ingredient2  = anIngredient()->withId($sameId)->withQuantity(2)->build();

        // Act
        $aggregatedIngredient = $ingredient1->aggregate($ingredient2);

        // Assert
        $this->assert ingredient has the right quantifier instance(Quantity::class, $aggregatedIngredient);
        $this->assert ingredient has added the quantifier(4, $aggregatedIngredient);
    }

    /**
     * @test
     * @dataProvider incompatibleIngredients
     *
     * @param int        $expectedErrorCode
     * @param Ingredient $ingredient1
     * @param Ingredient $ingredient2
     *
     * @throws Exception
     */
    public function an ingredient does not accept aggregation on another incompatible ingredient(
        int $expectedErrorCode,
        Ingredient $ingredient1,
        Ingredient $ingredient2
    ) {
        try {
            $ingredient1->aggregate($ingredient2);
        } catch (IngredientCannotBeAggregated $e) {
            $this->assertEquals($expectedErrorCode, $e->getCode());

            return;
        }

        throw new Exception(sprintf('%s exception should have been thrown', IngredientCannotBeAggregated::class));
    }

    /** @return array */
    public function incompatibleIngredients(): array
    {
        $id1 = Uuid::uuid4();
        $id2 = Uuid::uuid4();

        return [
            'Ingredients with different IDs' => [
                IngredientCannotBeAggregated::NOT_THE_SAME_ID,
                anIngredient()->withId($id1)->build(),
                anIngredient()->withId($id2)->build(),
            ],
            'Ingredients with different quantifier' => [
                IngredientCannotBeAggregated::NOT_THE_SAME_QUANTIFIER,
                anIngredient()->withId($id1)->withQuantity(2)->build(),
                anIngredient()->withId($id1)->withMass(3)->build(),
            ]
        ];
    }

    /** @return array */
    public function canAggregateIngredients(): array
    {
        $id1 = Uuid::uuid4();
        $id2 = Uuid::uuid4();

        return [
            'Can aggregate 2 masses ingredients with same ID' => [
                $ingredient1 = anIngredient()->withId($id1)->withMass(2.0)->build(),
                $ingredient2 = anIngredient()->withId($id1)->withMass(3.0)->build(),
                true
            ],
            'Cannot aggregate a mass & a quantity' => [
                $ingredient1 = anIngredient()->withId($id1)->withMass(2.0)->build(),
                $ingredient2 = anIngredient()->withId($id1)->withQuantity(2)->build(),
                false
            ],
            'Cannot aggregate 2 ingredients with different IDs' => [
                $ingredient1 = anIngredient()->withId($id1)->build(),
                $ingredient2 = anIngredient()->withId($id2)->build(),
                false
            ],
        ];
    }

    /**
     * @param string     $expectedClass
     * @param Ingredient $aggregatedIngredient
     *
     * @throws \PHPUnit\Framework\Exception
     */
    private function assert ingredient has the right quantifier instance(string $expectedClass, Ingredient $aggregatedIngredient)
    {
        $this->assertInstanceOf($expectedClass, $aggregatedIngredient->getQuantifier());
        $this->assertEquals(4, $aggregatedIngredient->getQuantifier()->toNativeUnit());
    }

    /**
     * @param float      $expectedQuantifier
     * @param Ingredient $aggregatedIngredient
     */
    private function assert ingredient has added the quantifier(float $expectedQuantifier, Ingredient $aggregatedIngredient)
    {
        $this->assertEquals($expectedQuantifier, $aggregatedIngredient->getQuantifier()->toNativeUnit());
    }
}

/** @return IngredientBuilder */
function anIngredient(): IngredientBuilder
{
    return new IngredientBuilder();
}