<?php
namespace FHuitelec\MealGenerator\Tests\Unit\System\Ingredient;

use FHuitelec\MealGenerator\Domain\Dish\Collection\Ingredients;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

class AggregateIngredientsTest extends TestCase
{
    /**
     * @test
     *
     * @throws \PHPUnit\Framework\Exception
     */
    public function it aggregates a list of ingredients()
    {
        // Arrange
        $tomatoId = Uuid::uuid4();
        $tunaId = Uuid::uuid4();

        $ingredients = Ingredients::fromArray([
            anIngredient()->withQuantity(2)->withId($tomatoId)->build(),
            anIngredient()->withQuantity(3)->withId($tomatoId)->build(),
            anIngredient()->withQuantity(2)->withId($tomatoId)->build(),
            anIngredient()->withMass(200)->withId($tunaId)->build(),
            anIngredient()->withMass(300)->withId($tunaId)->build(),
            anIngredient()->withMass(400)->withId($tunaId)->build(),
        ]);

        // Act
        $aggregatedIngredients = $ingredients->aggregate();

        // Assert
        $this->assertCount(2, $aggregatedIngredients->toArray());
        $this->assertEquals(7, $aggregatedIngredients->toArray()[0]->getQuantifier()->toNativeUnit());
        $this->assertEquals(900, $aggregatedIngredients->toArray()[1]->getQuantifier()->toNativeUnit());
    }
}