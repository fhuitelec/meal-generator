<?php
namespace FHuitelec\MealGenerator\Infrastructure\Store;

use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Domain\Meal\WeekMealStore;

class WeekMealsFsStore implements WeekMealStore
{
    /** @var string */
    private $storeRootPath;

    /**
     * @param string $storeRootPath
     */
    public function __construct(string $storeRootPath)
    {
        $this->storeRootPath = $storeRootPath;
    }

    /**
     * @param WeekMeals $weekMeals
     */
    public function store(WeekMeals $weekMeals): void
    {
        $filePath = $this->storeRootPath . '/' . $this->generateFileName($weekMeals);

        file_put_contents($filePath, serialize($weekMeals));
    }

    /**
     * @param WeekMeals $weekMeals
     *
     * @return string
     */
    private function generateFileName(WeekMeals $weekMeals): string
    {
        return $weekMeals->getCreatedAt()->format('Y-m-d_H\hi\ms\s') . '_' . (string)$weekMeals->getId();
    }
}