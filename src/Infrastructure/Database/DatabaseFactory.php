<?php
namespace FHuitelec\MealGenerator\Infrastructure\Database;

use Doctrine\MongoDB\Connection;
use Doctrine\MongoDB\Database;

class DatabaseFactory
{
    /** @var string */
    private $host;
    /** @var int */
    private $port;
    /** @var string */
    private $database;
    /** @var string */
    private $username;
    /** @var string */
    private $password;

    /**
     * @param string $host
     * @param int    $port
     * @param string $database
     * @param string $username
     * @param string $password
     */
    public function __construct(string $host, int $port, string $database, string $username, string $password)
    {
        $this->host = $host;
        $this->port = $port;
        $this->database = $database;
        $this->username = $username;
        $this->password = $password;
    }

    /** @return Database */
    public function create(): Database
    {
        $connection = new Connection(new \MongoClient(sprintf(
            'mongodb://%s:%s@%s:%d',
            $this->username,
            $this->password,
            $this->host,
            $this->port
        )));

        return $connection->selectDatabase($this->database);
    }
}