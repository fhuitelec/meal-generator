<?php
namespace FHuitelec\MealGenerator\Infrastructure;

use Doctrine\MongoDB\Collection;
use Doctrine\MongoDB\Database;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MongoDbTestCommand extends Command
{
    const COMMAND_NAME = 'mongodb';

    /** @var Collection */
    private $collection;

    /**
     * @param Database $database
     *
     * @throws LogicException
     */
    public function __construct(Database $database) {
        parent::__construct(self::COMMAND_NAME);

        $this->collection = $database->selectCollection('dishes');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws \Doctrine\MongoDB\Exception\ResultException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $match = $this->collection->createAggregationBuilder()
            ->match()
                ->field('name')
                ->equals('Pizzas')
                ->getExpression();
        $lookup = $this->collection->createAggregationBuilder()
            ->lookup('ingredients')
                ->localField('ingredients.ingredient.id')
                ->foreignField('_id')
                ->alias('ingredientsInfo')
                ->getExpression();
        $addFields = $this->collection->createAggregationBuilder()
            ->addFields()
                ->field('ingredients.ingredient')
                ->arrayElemAt('$ingredientsInfo', 0)
                ->getExpression();
        $projection = $this->collection->createAggregationBuilder()
            ->project()
                ->excludeFields(['ingredientsInfo'])
                ->getExpression();

        $test = $this->collection->aggregate([
            $match, $lookup, $addFields, $projection
        ], ['cursor' => true]);

        foreach ($test as $item) {
            dump($item);
        }

        return 0;
    }
}