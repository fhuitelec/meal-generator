<?php
namespace FHuitelec\MealGenerator\Infrastructure\Formatter;

use FHuitelec\MealGenerator\Domain\Dish\Collection\Ingredients;
use FHuitelec\MealGenerator\Domain\Dish\Ingredient;
use PhpUnitsOfMeasure\PhysicalQuantity\Mass;

class IngredientListFormatter
{
    /**
     * @param Ingredients $ingredients
     *
     * @return array
     */
    public function format(Ingredients $ingredients): array
    {
        return array_map(function (Ingredient $ingredient) {
            if ($ingredient->getQuantifier() instanceof Mass) {
                $quantifier = $ingredient->getQuantifier()->toUnit('grams') . 'g';
            } else {
                $quantifier = $ingredient->getQuantifier()->toNativeUnit();
            }

            return sprintf(
                '%s (%s)',
                $ingredient->getName(),
                $quantifier
            );
        }, $ingredients->toArray());
    }
}