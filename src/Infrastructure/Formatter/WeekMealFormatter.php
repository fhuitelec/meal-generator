<?php
namespace FHuitelec\MealGenerator\Infrastructure\Formatter;

use FHuitelec\MealGenerator\Domain\Meal\Collection\DayMeals;
use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use Symfony\Component\Translation\Exception\InvalidArgumentException;
use Symfony\Component\Translation\TranslatorInterface;

class WeekMealFormatter
{
    /** @var TranslatorInterface */
    private $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param WeekMeals $weekMeals
     *
     * @return array
     * @throws InvalidArgumentException
     */
    public function format(WeekMeals $weekMeals): array
    {
        $meals = $weekMeals->getMealsByDay();

        $headerArray = [[
            ' ',
            '<underline>' . ucfirst($this->translator->trans('lunch')) . '</underline>',
            '<underline>' . ucfirst($this->translator->trans('dinner')) . '</underline>'
        ]];
        $mealArray = array_reduce(array_keys($meals), function (array $carry, string $day) use ($meals) {
            $formattedDay = $this->formatDay($day);
            $formattedMeals = $this->formatMeals($meals[$day]);

            $carry[] = array_merge([$formattedDay], $formattedMeals);
            return $carry;
        }, []);

        return array_merge($headerArray, $mealArray);
    }

    /**
     * @param string $day
     *
     * @return string
     * @throws InvalidArgumentException
     */
    private function formatDay(string $day): string
    {
        return sprintf(
            '<light_red><underline>%s</underline></light_red>',
            ucfirst($this->translator->trans($day))
        );
    }

    /**
     * @param DayMeals $meals
     *
     * @return array
     */
    private function formatMeals(DayMeals $meals): array
    {
        return array_map(function (string $meal) {
            return sprintf('<light_green>%s</light_green>', $meal);
        }, $meals->toMealNameArray());
    }
}