<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Display\Banner;

use FHuitelec\MealGenerator\Infrastructure\Command\Display\Refreshable;
use GuzzleHttp\Promise\Promise;
use League\CLImate\CLImate;
use League\CLImate\Util\Writer\Buffer;

class NotificationBanner implements Refreshable
{
    /** @var CLImate */
    private $cli;
    /** @var array */
    private $notifications;

    /**
     * @param CLImate $cli
     * @param array   $notifications
     */
    public function __construct(CLImate $cli, array $notifications = [])
    {
        $this->cli = $cli;
        $this->notifications = [];
    }

    /**
     * @param string $notification
     */
    public function add(string $notification): void
    {
        $this->notifications[] = $notification;
    }

    /**
     * @throws \Exception
     */
    public function refresh(): void
    {
        if (0 === count($this->notifications)) {
            $this->cli->out('');
            $this->cli->out('');

            return;
        }

        /** @var Buffer $buffer */
        $buffer = array_reduce(
            $this->notifications,
            function (Buffer $buffer, string $notification) {
                $buffer->write($notification . ' ');
                return $buffer;
            },
            $this->cli->output->get('buffer')
        );

        $this->cli->out($buffer->get());
        $this->cli->out('');

        $buffer->clean();
        $this->dropCurrentNotifications();
    }

    private function dropCurrentNotifications(): void
    {
        $this->notifications = [];
    }
}