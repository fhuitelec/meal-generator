<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Display\Banner;

use FHuitelec\MealGenerator\Infrastructure\Command\Display\Refreshable;
use League\CLImate\CLImate;

class TitleBanner implements Refreshable
{
    /** @var CLImate */
    private $cli;

    /**
     * @param CLImate $cli
     */
    public function __construct(CLImate $cli)
    {
        $this->cli = $cli;
    }

    public function refresh(): void
    {
        $this->cli->out('Meal generator!');
        $this->cli->out('');
    }
}