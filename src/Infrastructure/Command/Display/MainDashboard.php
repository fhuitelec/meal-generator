<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Display;

use FHuitelec\MealGenerator\Infrastructure\Command\Display\Banner\NotificationBanner;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Banner\TitleBanner;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Dashboard\WeekMealsDashboard;
use League\CLImate\CLImate;

class MainDashboard implements Refreshable
{
    /** @var Refreshable[] */
    private $banners;
    /** @var Refreshable[] */
    private $refreshables;
    /** @var CLImate */
    private $cli;

    /**
     * @param CLImate            $cli
     * @param TitleBanner        $titleBanner
     * @param NotificationBanner $notificationBanner
     * @param WeekMealsDashboard $weekMealsDashboard
     */
    public function __construct(
        CLImate $cli,
        TitleBanner $titleBanner,
        NotificationBanner $notificationBanner,
        WeekMealsDashboard $weekMealsDashboard
    ) {
        $this->banners[] = $titleBanner;
        $this->banners[] = $notificationBanner;
        $this->refreshables[] = $weekMealsDashboard;
        $this->cli = $cli;
    }

    public function refresh(): void
    {
        $this->cli->clear();

        foreach ($this->banners as $dashboard) {
            $dashboard->refresh();
        }
        $this->cli->border();
        foreach ($this->refreshables as $dashboard) {
            $dashboard->refresh();
        }
        $this->cli->border();
    }
}