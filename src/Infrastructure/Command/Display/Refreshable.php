<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Display;

interface Refreshable
{
    public function refresh(): void;
}