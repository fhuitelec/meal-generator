<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Display\Dashboard;

use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Refreshable;
use FHuitelec\MealGenerator\Infrastructure\Formatter\WeekMealFormatter;
use League\CLImate\CLImate;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Translation\Exception\InvalidArgumentException;

class WeekMealsDashboard implements Refreshable
{
    /** @var SymfonyStyle */
    private $prompt;
    /** @var CLImate */
    private $cli;
    /** @var WeekMealFormatter */
    private $weekMealFormatter;
    /** @var WeekMeals */
    private $weekMeals;

    /**
     * @param SymfonyStyle      $prompt
     * @param CLImate           $cli
     * @param WeekMealFormatter $weekMealFormatter
     */
    public function __construct(
        SymfonyStyle $prompt,
        CLImate $cli,
        WeekMealFormatter $weekMealFormatter
    ) {
        $this->prompt = $prompt;
        $this->weekMealFormatter = $weekMealFormatter;
        $this->weekMeals = null;
        $this->cli = $cli;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function refresh(): void
    {
        if (null !== $this->weekMeals) {
            $this->cli->out('');
            $this->cli->shout('Menus');
            $this->cli->columns(
                $this->weekMealFormatter->format($this->weekMeals),
                2
            );
            $this->cli->out('');
        }
    }

    /**
     * @param WeekMeals $weekMeals
     */
    public function update(WeekMeals $weekMeals): void
    {
        $this->weekMeals = $weekMeals;
    }
}