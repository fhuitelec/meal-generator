<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Display\Event;

use Symfony\Component\EventDispatcher\Event;

class NewQuestionAsked extends Event
{
    const NAME = 'new_question_asked';
}