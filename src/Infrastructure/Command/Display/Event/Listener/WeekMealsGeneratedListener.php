<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\Listener;

use FHuitelec\MealGenerator\Infrastructure\Command\Display\Banner\NotificationBanner;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Dashboard\WeekMealsDashboard;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\WeekMealsGenerated;

class WeekMealsGeneratedListener
{
    /** @var WeekMealsDashboard */
    private $weekMealsDashboard;
    /** @var NotificationBanner */
    private $notificationBanner;

    /**
     * @param WeekMealsDashboard $weekMealsDashboard
     * @param NotificationBanner $notificationBanner
     */
    public function __construct(
        WeekMealsDashboard $weekMealsDashboard,
        NotificationBanner $notificationBanner
    ) {
        $this->weekMealsDashboard = $weekMealsDashboard;
        $this->notificationBanner = $notificationBanner;
    }

    /**
     * @param WeekMealsGenerated $event
     */
    public function onWeekMealsGenerated(WeekMealsGenerated $event): void
    {
        $this->weekMealsDashboard->update($event->getNewWeekMeals());
        $this->notificationBanner->add('Menus de la semaine généré');
    }
}