<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\Listener;

use FHuitelec\MealGenerator\Infrastructure\Command\Display\Banner\NotificationBanner;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Dashboard\WeekMealsDashboard;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\MealReplaced;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\WeekMealsGenerated;

class MealReplacedListener
{
    /** @var WeekMealsDashboard */
    private $weekMealsDashboard;
    /** @var NotificationBanner */
    private $notificationBanner;

    /**
     * @param WeekMealsDashboard $weekMealsDashboard
     * @param NotificationBanner $notificationBanner
     */
    public function __construct(
        WeekMealsDashboard $weekMealsDashboard,
        NotificationBanner $notificationBanner
    ) {
        $this->weekMealsDashboard = $weekMealsDashboard;
        $this->notificationBanner = $notificationBanner;
    }

    /**
     * @param MealReplaced $event
     */
    public function onMealReplaced(MealReplaced $event): void
    {
        $this->weekMealsDashboard->update($event->getNewWeekMeals());
        $this->notificationBanner->add('Repas modifié avec succès !');
    }
}