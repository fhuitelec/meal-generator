<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\Listener;

use FHuitelec\MealGenerator\Infrastructure\Command\Display\MainDashboard;
use Symfony\Component\EventDispatcher\Event;

class RefreshListener
{
    /** @var MainDashboard */
    private $mainDashboard;

    /**
     * @param MainDashboard $mainDashboard
     */
    public function __construct(MainDashboard $mainDashboard)
    {
        $this->mainDashboard = $mainDashboard;
    }

    public function refresh(Event $_): void
    {
        $this->mainDashboard->refresh();
    }
}