<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Display\Event;

use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use Symfony\Component\EventDispatcher\Event;

class WeekMealsGenerated extends Event
{
    const NAME = 'week_meals.generated';

    /** @var WeekMeals */
    private $newWeekMeals;

    /**
     * @param WeekMeals $newWeekMeals
     */
    public function __construct(WeekMeals $newWeekMeals)
    {
        $this->newWeekMeals = $newWeekMeals;
    }

    /** @return WeekMeals */
    public function getNewWeekMeals(): WeekMeals
    {
        return $this->newWeekMeals;
    }
}