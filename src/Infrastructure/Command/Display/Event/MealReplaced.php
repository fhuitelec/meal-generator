<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Display\Event;

use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use Symfony\Component\EventDispatcher\Event;

class MealReplaced extends Event
{
    const NAME = 'week_meals.replaced';

    /** @var WeekMeals */
    private $newWeekMeals;

    /**
     * @param WeekMeals $newWeekMeals
     * TODO: Add replaced meal for notifications
     */
    public function __construct(WeekMeals $newWeekMeals)
    {
        $this->newWeekMeals = $newWeekMeals;
    }

    /** @return WeekMeals */
    public function getNewWeekMeals(): WeekMeals
    {
        return $this->newWeekMeals;
    }
}