<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command;

use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Infrastructure\Command\Task\DisplayGroceryList;
use FHuitelec\MealGenerator\Infrastructure\Command\Task\GenerateWeekMeals;
use FHuitelec\MealGenerator\Infrastructure\Command\Task\ReplaceMeals;
use FHuitelec\MealGenerator\Infrastructure\Command\Task\Subtask\MealToReplaceInput;
use League\Tactician\Exception\InvalidCommandException;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Exception\LogicException;

class Tasks
{
    /** @var GenerateWeekMeals  */
    private $generateWeekMeals;
    /** @var DisplayGroceryList */
    private $displayGroceryList;
    /** @var ReplaceMeals */
    private $replaceMeals;

    /**
     * @param GenerateWeekMeals  $generateWeekMeals
     * @param DisplayGroceryList $displayGroceryList
     * @param ReplaceMeals       $replaceMeals
     */
    public function __construct(
        GenerateWeekMeals $generateWeekMeals,
        DisplayGroceryList $displayGroceryList,
        ReplaceMeals $replaceMeals
    ) {
        $this->generateWeekMeals = $generateWeekMeals;
        $this->displayGroceryList = $displayGroceryList;
        $this->replaceMeals = $replaceMeals;
    }

    /**
     * @return WeekMeals
     * @throws InvalidCommandException
     */
    public function generateWeekMeals(): WeekMeals
    {
        return $this->generateWeekMeals->execute();
    }

    /**
     * @param WeekMeals $weekMeals
     */
    public function displayGroceryList(WeekMeals $weekMeals): void
    {
        $this->displayGroceryList->execute($weekMeals);
    }

    /**
     * @param WeekMeals $weekMeals
     *
     * @return WeekMeals
     * @throws InvalidCommandException
     * @throws InvalidArgumentException|LogicException
     */
    public function replaceMeals(WeekMeals $weekMeals): WeekMeals
    {
        return $this->replaceMeals->execute($weekMeals);
    }
}