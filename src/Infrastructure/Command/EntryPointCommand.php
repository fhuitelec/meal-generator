<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command;

use Doctrine\MongoDB\Collection;
use Doctrine\MongoDB\Database;
use League\Tactician\Exception\InvalidCommandException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EntryPointCommand extends Command
{
    const COMMAND_NAME = 'meals';

    /** @var Tasks */
    private $cliTask;
    /** @var Collection */
    private $collection;

    /**
     * @param Tasks    $cliTask
     *
     * @param Database $database
     *
     * @throws LogicException
     */
    public function __construct(
        Tasks $cliTask,
        Database $database
    ) {
        parent::__construct(self::COMMAND_NAME);

        $this->cliTask = $cliTask;
        $this->collection = $database->selectCollection('test');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws InvalidArgumentException
     * @throws LogicException|InvalidCommandException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $weekMeals = $this->cliTask->generateWeekMeals();
        $weekMeals = $this->cliTask->replaceMeals($weekMeals);
        $this->cliTask->displayGroceryList($weekMeals);

        return 0;
    }
}