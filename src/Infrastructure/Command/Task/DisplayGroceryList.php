<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Task;

use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Infrastructure\Formatter\IngredientListFormatter;
use League\CLImate\CLImate;
use Symfony\Component\Console\Style\SymfonyStyle;

class DisplayGroceryList
{
    /** @var SymfonyStyle */
    private $prompt;
    /** @var CLImate */
    private $cli;
    /** @var IngredientListFormatter */
    private $formatter;

    /**
     * @param SymfonyStyle       $prompt
     * @param CLImate           $cli
     * @param IngredientListFormatter $formatter
     */
    public function __construct(SymfonyStyle $prompt, CLImate $cli, IngredientListFormatter $formatter)
    {
        $this->prompt = $prompt;
        $this->cli = $cli;
        $this->formatter = $formatter;
    }

    /**
     * @param WeekMeals $mealsPlan
     */
    public function execute(WeekMeals $mealsPlan)
    {
        if (0 < count($mealsPlan->getIngredients()->toArray())) {
            $this->prompt->section('Liste de courses');
            $this->cli->columns(
                $this->formatter->format($mealsPlan->getIngredients()),
                4
            );
        }
    }
}