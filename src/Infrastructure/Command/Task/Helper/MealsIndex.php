<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Task\Helper;

use Assert\Assert;
use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Domain\Meal\Day;

class MealsIndex
{
    /** @var string[] */
    private $dayAndMomentIndex;
    /** @var string[] */
    private $dishNameIndex;

    /**
     * @param WeekMeals $weekMeals
     */
    public function __construct(WeekMeals $weekMeals)
    {
        $this->dayAndMomentIndex = [
            (string)$weekMeals->getByDay(Day::MONDAY)->getLunch()->getId() => 'Lundi - midi',
            (string)$weekMeals->getByDay(Day::MONDAY)->getDinner()->getId() => 'Lundi - soir',
            (string)$weekMeals->getByDay(Day::TUESDAY)->getLunch()->getId() => 'Mardi - midi',
            (string)$weekMeals->getByDay(Day::TUESDAY)->getDinner()->getId() => 'Mardi - soir',
            (string)$weekMeals->getByDay(Day::WEDNESDAY)->getLunch()->getId() => 'Mercredi - midi',
            (string)$weekMeals->getByDay(Day::WEDNESDAY)->getDinner()->getId() => 'Mercredi - soir',
            (string)$weekMeals->getByDay(Day::THURSDAY)->getLunch()->getId() => 'Jeudi - midi',
            (string)$weekMeals->getByDay(Day::THURSDAY)->getDinner()->getId() => 'Jeudi - soir',
            (string)$weekMeals->getByDay(Day::FRIDAY)->getLunch()->getId() => 'Vendredi - midi',
            (string)$weekMeals->getByDay(Day::FRIDAY)->getDinner()->getId() => 'Vendredi - soir',
            (string)$weekMeals->getByDay(Day::SATURDAY)->getLunch()->getId() => 'Samedi - midi',
            (string)$weekMeals->getByDay(Day::SATURDAY)->getDinner()->getId() => 'Samedi - soir',
            (string)$weekMeals->getByDay(Day::SUNDAY)->getLunch()->getId() => 'Dimanche - midi',
            (string)$weekMeals->getByDay(Day::SUNDAY)->getDinner()->getId() => 'Dimanche - soir',
        ];

        $this->dishNameIndex = [
            (string)$weekMeals->getByDay(Day::MONDAY)->getLunch()->getId() => $weekMeals->getByDay(Day::MONDAY)->getLunch()->getDishName(),
            (string)$weekMeals->getByDay(Day::MONDAY)->getDinner()->getId() => $weekMeals->getByDay(Day::MONDAY)->getDinner()->getDishName(),
            (string)$weekMeals->getByDay(Day::TUESDAY)->getLunch()->getId() => $weekMeals->getByDay(Day::TUESDAY)->getLunch()->getDishName(),
            (string)$weekMeals->getByDay(Day::TUESDAY)->getDinner()->getId() => $weekMeals->getByDay(Day::TUESDAY)->getDinner()->getDishName(),
            (string)$weekMeals->getByDay(Day::WEDNESDAY)->getLunch()->getId() => $weekMeals->getByDay(Day::WEDNESDAY)->getLunch()->getDishName(),
            (string)$weekMeals->getByDay(Day::WEDNESDAY)->getDinner()->getId() => $weekMeals->getByDay(Day::WEDNESDAY)->getDinner()->getDishName(),
            (string)$weekMeals->getByDay(Day::THURSDAY)->getLunch()->getId() => $weekMeals->getByDay(Day::THURSDAY)->getLunch()->getDishName(),
            (string)$weekMeals->getByDay(Day::THURSDAY)->getDinner()->getId() => $weekMeals->getByDay(Day::THURSDAY)->getDinner()->getDishName(),
            (string)$weekMeals->getByDay(Day::FRIDAY)->getLunch()->getId() => $weekMeals->getByDay(Day::FRIDAY)->getLunch()->getDishName(),
            (string)$weekMeals->getByDay(Day::FRIDAY)->getDinner()->getId() => $weekMeals->getByDay(Day::FRIDAY)->getDinner()->getDishName(),
            (string)$weekMeals->getByDay(Day::SATURDAY)->getLunch()->getId() => $weekMeals->getByDay(Day::SATURDAY)->getLunch()->getDishName(),
            (string)$weekMeals->getByDay(Day::SATURDAY)->getDinner()->getId() => $weekMeals->getByDay(Day::SATURDAY)->getDinner()->getDishName(),
            (string)$weekMeals->getByDay(Day::SUNDAY)->getLunch()->getId() => $weekMeals->getByDay(Day::SUNDAY)->getLunch()->getDishName(),
            (string)$weekMeals->getByDay(Day::SUNDAY)->getDinner()->getId() => $weekMeals->getByDay(Day::SUNDAY)->getDinner()->getDishName(),
        ];
    }

    /**
     * @param WeekMeals $weekMeals
     *
     * @return self
     */
    public static function createIndex(WeekMeals $weekMeals): self
    {
        return new self($weekMeals);
    }

    /** @return string[] */
    public function getDayAndMomentIndex(): array
    {
        return $this->dayAndMomentIndex;
    }

    /**
     * @param string $id
     *
     * @return string|null
     */
    public function getDayAndMomentByMealId(string $id): ?string
    {
        if (!array_key_exists($id, $this->dayAndMomentIndex)) {
            return null;
        }

        return $this->dayAndMomentIndex[$id];
    }

    /**
     * @param string $id
     *
     * @return string|null
     */
    public function getDishNameByMealId(string $id): ?string
    {
        if (!array_key_exists($id, $this->dishNameIndex)) {
            return null;
        }

        return $this->dishNameIndex[$id];
    }
}