<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Task;

use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\NewQuestionAsked;
use FHuitelec\MealGenerator\Infrastructure\Command\Task\Subtask\MealToReplaceInput;
use League\Tactician\Exception\InvalidCommandException;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ReplaceMeals
{
    /** @var MealToReplaceInput */
    private $mealToReplaceInput;
    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /**
     * @param MealToReplaceInput       $mealToReplaceInput
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        MealToReplaceInput $mealToReplaceInput,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->mealToReplaceInput = $mealToReplaceInput;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param WeekMeals $weekMeals
     *
     * @return WeekMeals
     * @throws InvalidCommandException
     * @throws InvalidArgumentException|LogicException
     */
    public function execute(WeekMeals $weekMeals): WeekMeals
    {
        do {
            $this->eventDispatcher->dispatch(NewQuestionAsked::NAME);
            $newWeekMeals = $this->mealToReplaceInput->execute($weekMeals);

            if (null === $newWeekMeals) {
                break;
            }

            $weekMeals = $newWeekMeals;
        } while (true);

        return $weekMeals;
    }
}