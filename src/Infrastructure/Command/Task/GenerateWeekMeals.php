<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Task;

use FHuitelec\MealGenerator\Domain\Command\GenerateWeekMealsCommand;
use FHuitelec\MealGenerator\Domain\Command\SaveWeekMealsCommand;
use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\WeekMealsGenerated;
use League\Tactician\CommandBus;
use League\Tactician\Exception\InvalidCommandException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class GenerateWeekMeals
{
    /** @var CommandBus */
    private $commandBus;
    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /**
     * @param CommandBus      $commandBus
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        CommandBus $commandBus,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->commandBus = $commandBus;
    }
    /**
     * @return WeekMeals
     * @throws InvalidCommandException
     */
    public function execute(): WeekMeals
    {
        $weekMeals = $this->commandBus->handle(new GenerateWeekMealsCommand());
        $this->commandBus->handle(new SaveWeekMealsCommand($weekMeals));
        $this->eventDispatcher->dispatch(WeekMealsGenerated::NAME, new WeekMealsGenerated($weekMeals));

        return $weekMeals;
    }
}