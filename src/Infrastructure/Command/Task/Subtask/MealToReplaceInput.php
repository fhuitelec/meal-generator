<?php
namespace FHuitelec\MealGenerator\Infrastructure\Command\Task\Subtask;

use FHuitelec\MealGenerator\Domain\Command\GetAllDishesQuery;
use FHuitelec\MealGenerator\Domain\Command\ReplaceAWeekMealCommand;
use FHuitelec\MealGenerator\Domain\Dish\Collection\Dishes;
use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Domain\Meal\Identity\MealId;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\MealReplaced;
use FHuitelec\MealGenerator\Infrastructure\Command\Display\Event\WeekMealsGenerated;
use FHuitelec\MealGenerator\Infrastructure\Command\Task\Helper\MealsIndex;
use League\CLImate\CLImate;
use League\Tactician\CommandBus;
use League\Tactician\Exception\InvalidCommandException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Exception\LogicException;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class MealToReplaceInput
{
    /** @var SymfonyStyle */
    private $prompt;
    /** @var CLImate */
    private $cli;
    /** @var CommandBus */
    private $commandBus;
    /** @var EventDispatcherInterface */
    private $eventDispatcher;

    /**
     * @param SymfonyStyle             $prompt
     * @param CLImate                  $cli
     * @param CommandBus               $commandBus
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        SymfonyStyle $prompt,
        CLImate $cli,
        CommandBus $commandBus,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->prompt = $prompt;
        $this->cli = $cli;
        $this->commandBus = $commandBus;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param WeekMeals $weekMeals
     *
     * @return WeekMeals|null
     * @throws InvalidArgumentException
     * @throws InvalidCommandException|LogicException
     */
    public function execute(WeekMeals $weekMeals): ?WeekMeals
    {
        $this->cli->out('');
        $mealsIndex = MealsIndex::createIndex($weekMeals);
        $input = $this->cli->radio('Quel repas souhaitez-vous modifier ?', array_merge(
            ['stop' => 'Aucun'],
            $mealsIndex->getDayAndMomentIndex()
        ));
        $mealId = $input->prompt();

        if (null === $mealId || 'stop' === $mealId) {
            return null;
        }

        /** @var Dishes $dishes */
        $dishes = $this->commandBus->handle(new GetAllDishesQuery());

        // TODO: Utiliser une factory pour les questions avec le translator
        $dishName = $this->prompt->askQuestion(
            (new Question(sprintf(
                'Par quoi souhaitez-vous modifier le repas de %s (%s)',
                $mealsIndex->getDayAndMomentByMealId($mealId),
                $mealsIndex->getDishNameByMealId($mealId)
            ), 'Annuler'))->setAutocompleterValues(array_merge(
                    ['Annuler'],
                    $dishes->toDishNameArray()
            ))
        );

        // TODO: créer un objet Hang qui fera office de prompt par défaut et attend une action
        if ('annuler' === $dishName || null === $dishName) {
            return null;
        }

        $replacingDish = $dishes->getByName($dishName);

        $command = new ReplaceAWeekMealCommand(
            new MealId(Uuid::fromString($mealId)),
            $replacingDish,
            $weekMeals
        );

        /** @var WeekMeals $newWeekMeals */
        $newWeekMeals = $this->commandBus->handle($command);

        $this->eventDispatcher->dispatch(
            MealReplaced::NAME,
            new MealReplaced($newWeekMeals)
        );

        return $newWeekMeals;
    }
}