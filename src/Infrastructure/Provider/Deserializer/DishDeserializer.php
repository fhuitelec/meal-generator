<?php
namespace FHuitelec\MealGenerator\Infrastructure\Provider\Deserializer;

use FHuitelec\MealGenerator\Domain\Dish\Dish;
use FHuitelec\MealGenerator\Domain\Dish\Identity\DishId;
use Ramsey\Uuid\Uuid;

class DishDeserializer
{
    /** @var IngredientDeserializer */
    private $ingredientDeserializer;

    /**
     * @param IngredientDeserializer $ingredientDeserializer
     */
    public function __construct(IngredientDeserializer $ingredientDeserializer)
    {
        $this->ingredientDeserializer = $ingredientDeserializer;
    }

    /**
     * @param array $dish
     *
     * @return Dish
     * @throws \InvalidArgumentException
     */
    public function deserialize(array $dish): Dish
    {
        if (!array_key_exists('id', $dish)) {
            throw new \InvalidArgumentException('Dish has no ID');
        }

        $id = Uuid::fromString($dish['id']);

        if (!array_key_exists('name', $dish)) {
            throw new \InvalidArgumentException(sprintf('Dish with ID \'%s\' has no name', $id));
        }

        $canBeGenerated = true;
        if (array_key_exists('can-be-generated', $dish)) {
            $canBeGenerated = (bool) $dish['can-be-generated'];
        }

        $name = $dish['name'];
        $ingredients = $this->ingredientDeserializer->deserialize($dish, $name);

        return new Dish(new DishId($id), $name, $ingredients, $canBeGenerated);
    }
}