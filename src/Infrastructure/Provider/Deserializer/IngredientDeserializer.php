<?php
namespace FHuitelec\MealGenerator\Infrastructure\Provider\Deserializer;

use FHuitelec\MealGenerator\Domain\Dish\Collection\Ingredients;
use FHuitelec\MealGenerator\Domain\Dish\Identity\IngredientId;
use FHuitelec\MealGenerator\Domain\Dish\Ingredient;
use PhpUnitsOfMeasure\PhysicalQuantity\Mass;
use PhpUnitsOfMeasure\PhysicalQuantity\Quantity;
use Ramsey\Uuid\Uuid;

class IngredientDeserializer
{
    const QUANTIFIER_MASS = 'mass';
    const QUANTIFIER_QUANTITY = 'quantity';

    /** @var array */
    private $ingredients;

    /**
     * @param array $ingredients
     */
    public function __construct(array $ingredients)
    {
        $this->ingredients = $ingredients;
    }

    /**
     * @param array  $dish
     * @param string $dishName
     *
     * @return Ingredients
     */
    public function deserialize(array $dish, $dishName): Ingredients
    {
        if (!array_key_exists('ingredients', $dish)) {
            return Ingredients::fromArray([]);
        }

        $ingredients = array_map(function (int $index, array $ingredient) use ($dishName) {
            return $this->deserializeSingleIngredient($ingredient, $index, $dishName);
        }, array_keys($dish['ingredients']), $dish['ingredients']);

        return Ingredients::fromArray($ingredients);
    }

    /**
     * @param array  $ingredient
     * @param int    $index
     * @param string $dishName
     *
     * @return Ingredient
     * @throws \InvalidArgumentException
     * @throws \PhpUnitsOfMeasure\Exception\NonNumericValue
     * @throws \PhpUnitsOfMeasure\Exception\NonStringUnitName
     */
    private function deserializeSingleIngredient(array $ingredient, $index, $dishName): Ingredient
    {
        if (!array_key_exists('id', $ingredient)) {
            throw new \InvalidArgumentException(sprintf('Ingredient #%d of dish \'%s\' has no ID', $index + 1, $dishName));
        }

        $ingredientId = $ingredient['id'];
        $ingredientName = $this->ingredients[$ingredientId]['name'];

        if (!array_key_exists('quantifier', $ingredient)) {
            throw new \InvalidArgumentException(sprintf('Ingredient \'%s\' of dish \'%s\' has no quantifier', $ingredientName, $dishName));
        }

        $quantifierId = array_keys($ingredient['quantifier'])[0];
        $quantifierPayload = $ingredient['quantifier'];

        switch ($quantifierId) {
            case self::QUANTIFIER_MASS:
                $ingredientQuantifier = new Mass(
                    (float)$quantifierPayload[ self::QUANTIFIER_MASS ]['quantity'],
                    $quantifierPayload[ self::QUANTIFIER_MASS ]['unit']
                );
                break;
            case self::QUANTIFIER_QUANTITY:
                $ingredientQuantifier = new Quantity(
                    (float)$quantifierPayload[ self::QUANTIFIER_QUANTITY ],
                    'mol'
                );
                break;
            default:
                throw new \InvalidArgumentException(sprintf('Unknown quantifier %s', $quantifierPayload));
        }

        return new Ingredient(new IngredientId(Uuid::fromString($ingredientId)), $ingredientName, $ingredientQuantifier);
    }
}