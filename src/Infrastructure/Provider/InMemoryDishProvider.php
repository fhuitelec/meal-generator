<?php
namespace FHuitelec\MealGenerator\Infrastructure\Provider;

use FHuitelec\MealGenerator\Domain\Dish\Collection\Dishes;
use FHuitelec\MealGenerator\Domain\Dish\DishProvider;
use FHuitelec\MealGenerator\Infrastructure\Provider\Deserializer\DishDeserializer;

class InMemoryDishProvider implements DishProvider
{
    /** @var array */
    private $dishes;
    /** @var DishDeserializer */
    private $deserializer;

    /**
     * @param array            $dishes
     * @param DishDeserializer $deserializer
     */
    public function __construct(array $dishes, DishDeserializer $deserializer)
    {
        $this->dishes = $dishes;
        $this->deserializer = $deserializer;
    }

    /** @return Dishes */
    public function findAllDishes(): Dishes
    {
        $dishes = array_map(function (array $dish) {
            return $this->deserializer->deserialize($dish);
        }, $this->dishes);

        return Dishes::fromArray($dishes);
    }
}