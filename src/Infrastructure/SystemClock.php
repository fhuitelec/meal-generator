<?php
namespace FHuitelec\MealGenerator\Infrastructure;

use FHuitelec\MealGenerator\Domain\Clock;

class SystemClock implements Clock
{
    /** @return \DateTimeImmutable */
    public function now(): \DateTimeImmutable
    {
        return new \DateTimeImmutable();
    }
}