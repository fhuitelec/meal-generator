<?php
namespace FHuitelec\MealGenerator\Domain\Command\Handler;

use FHuitelec\MealGenerator\Domain\Command\GetAllDishesQuery;
use FHuitelec\MealGenerator\Domain\Dish\Collection\Dishes;
use FHuitelec\MealGenerator\Domain\Dish\DishProvider;

class GetAllDishesHandler
{
    /** @var DishProvider */
    private $dishProvider;

    /**
     * @param DishProvider $dishProvider
     */
    public function __construct(DishProvider $dishProvider)
    {
        $this->dishProvider = $dishProvider;
    }

    /**
     * @param GetAllDishesQuery $_
     *
     * @return Dishes
     */
    public function handle(GetAllDishesQuery $_): Dishes
    {
        return $this->dishProvider->findAllDishes();
    }
}