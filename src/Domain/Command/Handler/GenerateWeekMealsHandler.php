<?php
namespace FHuitelec\MealGenerator\Domain\Command\Handler;

use FHuitelec\MealGenerator\Domain\Clock;
use FHuitelec\MealGenerator\Domain\Command\GenerateWeekMealsCommand;
use FHuitelec\MealGenerator\Domain\Dish\Exception\NotEnoughDishesFound;
use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Domain\Meal\MealGenerator;

class GenerateWeekMealsHandler
{
    /** @var MealGenerator */
    private $mealGenerator;

    /**
     * @param MealGenerator $mealGenerator
     */
    public function __construct(MealGenerator $mealGenerator)
    {
        $this->mealGenerator = $mealGenerator;
    }

    /**
     * @param GenerateWeekMealsCommand $_
     *
     * @return WeekMeals
     * @throws NotEnoughDishesFound
     */
    public function handle(GenerateWeekMealsCommand $_): WeekMeals
    {
        return $this->mealGenerator->generateWeek();
    }
}