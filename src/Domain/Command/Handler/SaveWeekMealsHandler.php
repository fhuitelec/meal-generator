<?php
namespace FHuitelec\MealGenerator\Domain\Command\Handler;

use FHuitelec\MealGenerator\Domain\Command\SaveWeekMealsCommand;
use FHuitelec\MealGenerator\Domain\Meal\WeekMealStore;

class SaveWeekMealsHandler
{
    /** @var WeekMealStore */
    private $weekMealStore;

    /**
     * @param WeekMealStore $weekMealStore
     */
    public function __construct(WeekMealStore $weekMealStore)
    {
        $this->weekMealStore = $weekMealStore;
    }

    /**
     * @param SaveWeekMealsCommand $command
     */
    public function handle(SaveWeekMealsCommand $command): void
    {
        $this->weekMealStore->store($command->getWeekMeals());
    }
}