<?php
namespace FHuitelec\MealGenerator\Domain\Command\Handler;

use FHuitelec\MealGenerator\Domain\Command\ReplaceAWeekMealCommand;
use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Domain\Meal\WeekMealStore;

class ReplaceAWeekMealHandler
{
    /** @var WeekMealStore */
    private $weekMealStore;

    /**
     * @param WeekMealStore $weekMealStore
     */
    public function __construct(WeekMealStore $weekMealStore)
    {
        $this->weekMealStore = $weekMealStore;
    }

    /**
     * @param ReplaceAWeekMealCommand $command
     *
     * @return WeekMeals
     */
    public function handle(ReplaceAWeekMealCommand $command): WeekMeals
    {
        $weekMeals = $command->getWeekMeals()->replaceMeal(
            $command->getMealId(),
            $command->getNewDish()
        );

        $this->weekMealStore->store($weekMeals);

        return $weekMeals;
    }
}