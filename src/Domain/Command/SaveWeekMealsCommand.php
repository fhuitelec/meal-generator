<?php
namespace FHuitelec\MealGenerator\Domain\Command;

use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;

class SaveWeekMealsCommand
{
    /** @var WeekMeals */
    private $weekMeals;

    /**
     * @param WeekMeals $weekMeals
     */
    public function __construct(WeekMeals $weekMeals)
    {
        $this->weekMeals = $weekMeals;
    }

    /** @return WeekMeals */
    public function getWeekMeals(): WeekMeals
    {
        return $this->weekMeals;
    }
}