<?php
namespace FHuitelec\MealGenerator\Domain\Command;

use FHuitelec\MealGenerator\Domain\Dish\Dish;
use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Domain\Meal\Identity\MealId;
use FHuitelec\MealGenerator\Domain\Meal\Meal;

class ReplaceAWeekMealCommand
{
    /** @var MealId */
    private $mealId;
    /** @var Dish */
    private $newDish;
    /** @var WeekMeals */
    private $weekMeals;

    /**
     * @param MealId    $mealId
     * @param Dish      $newDish
     * @param WeekMeals $weekMeals
     */
    public function __construct(MealId $mealId, Dish $newDish, WeekMeals $weekMeals)
    {
        $this->mealId = $mealId;
        $this->newDish = $newDish;
        $this->weekMeals = $weekMeals;
    }

    /** @return MealId */
    public function getMealId(): MealId
    {
        return $this->mealId;
    }

    /** @return Dish */
    public function getNewDish(): Dish
    {
        return $this->newDish;
    }

    /** @return WeekMeals */
    public function getWeekMeals(): WeekMeals
    {
        return $this->weekMeals;
    }
}