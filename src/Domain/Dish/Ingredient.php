<?php
namespace FHuitelec\MealGenerator\Domain\Dish;

use FHuitelec\MealGenerator\Domain\Dish\Exception\IngredientCannotBeAggregated;
use FHuitelec\MealGenerator\Domain\Dish\Identity\IngredientId;
use PhpUnitsOfMeasure\Exception\PhysicalQuantityMismatch;
use PhpUnitsOfMeasure\PhysicalQuantityInterface;

class Ingredient
{
    /** @var IngredientId */
    private $id;
    /** @var string */
    private $name;
    /** @var PhysicalQuantityInterface */
    private $quantifier;

    /**
     * @param IngredientId              $id
     * @param string                    $name
     * @param PhysicalQuantityInterface $quantity
     */
    public function __construct(
        IngredientId $id,
        string $name,
        PhysicalQuantityInterface $quantity
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->quantifier = $quantity;
    }

    /**
     * @param self $anotherIngredient
     *
     * @return bool
     */
    public function canAggregate(self $anotherIngredient): bool
    {
        return $this->hasTheSameId($anotherIngredient) && $this->hasTheSameQuantifier($anotherIngredient);
    }

    /**
     * @param Ingredient $anotherIngredient
     *
     * @return bool
     */
    private function hasTheSameId(self $anotherIngredient): bool
    {
        return $this->id->equals($anotherIngredient->id);
    }

    /**
     * @param Ingredient $anotherIngredient
     *
     * @return bool
     */
    private function hasTheSameQuantifier(self $anotherIngredient): bool
    {
        return $this->quantifier->isEquivalentQuantity($anotherIngredient->quantifier);
    }

    /**
     * @param self $anotherIngredient
     *
     * @return self
     * @throws IngredientCannotBeAggregated
     */
    public function aggregate(self $anotherIngredient): self
    {
        if (!$this->hasTheSameId($anotherIngredient)) {
            throw IngredientCannotBeAggregated::notTheSameId($this, $anotherIngredient);
        }

        try {
            $aggregatedQuantifier = $this->quantifier->add($anotherIngredient->quantifier);
        } catch (PhysicalQuantityMismatch $e) {
            throw IngredientCannotBeAggregated::notTheSameQuantifier($this, $anotherIngredient);
        }

        return new self(
            $this->id,
            $this->name,
            $aggregatedQuantifier
        );
    }

    /** @return IngredientId */
    public function getId(): IngredientId
    {
        return $this->id;
    }

    /** @return string */
    public function getName(): string
    {
        return $this->name;
    }

    /** @return PhysicalQuantityInterface */
    public function getQuantifier(): PhysicalQuantityInterface
    {
        return $this->quantifier;
    }
}