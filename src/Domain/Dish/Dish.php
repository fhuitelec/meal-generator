<?php
namespace FHuitelec\MealGenerator\Domain\Dish;

use FHuitelec\MealGenerator\Domain\Dish\Collection\Ingredients;
use FHuitelec\MealGenerator\Domain\Dish\Identity\DishId;

class Dish
{
    /** @var DishId */
    private $id;
    /** @var string */
    private $name;
    /** @var Ingredients */
    private $ingredients;
    /** @var bool */
    private $canBeGenerated;

    /**
     * @param DishId      $id
     * @param string      $name
     * @param Ingredients $ingredients
     * @param bool        $canBeGenerated
     */
    public function __construct(
        DishId $id,
        string $name,
        Ingredients $ingredients,
        bool $canBeGenerated
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->ingredients = $ingredients;
        $this->canBeGenerated = $canBeGenerated;
    }

    /** @return DishId */
    public function getId(): DishId
    {
        return $this->id;
    }

    /** @return string */
    public function getName(): string
    {
        return $this->name;
    }

    /** @return Ingredients */
    public function getIngredients(): Ingredients
    {
        return $this->ingredients;
    }

    /** @return bool */
    public function canBeGenerated(): bool
    {
        return $this->canBeGenerated;
    }
}