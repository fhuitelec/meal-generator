<?php
namespace FHuitelec\MealGenerator\Domain\Dish\Collection;

use Assert\Assert;
use FHuitelec\MealGenerator\Domain\Dish\Dish;

class Dishes
{
    /** @var array  */
    private $dishes;

    /**
     * @param array $meals
     */
    private function __construct(array $meals)
    {
        Assert::thatAll($meals)->isInstanceOf(Dish::class);

        $this->dishes = $meals;
    }

    /**
     * @param array $meals
     *
     * @return self
     */
    public static function fromArray(array $meals): self
    {
        return new self($meals);
    }

    /** @return array */
    public function toArray()
    {
        return $this->dishes;
    }

    /**
     * @param string $dishName
     *
     * @return Dish|null
     */
    public function getByName(string $dishName): ?Dish
    {
        $foundDishes = array_filter($this->dishes, function (Dish $dish) use ($dishName) {
            return $dish->getName() === $dishName;
        });

        return (0 < count($foundDishes)) ? array_values($foundDishes)[0] : null;
    }

    /** @return array */
    public function toDishNameArray(): array
    {
        return array_reduce($this->dishes, function (array $index, Dish $dish) {
            $index[(string)$dish->getId()] = $dish->getName();
            return $index;
        }, []);
    }
}