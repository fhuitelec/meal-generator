<?php
namespace FHuitelec\MealGenerator\Domain\Dish\Collection;

use Assert\Assert;
use FHuitelec\MealGenerator\Domain\Dish\Ingredient;

class Ingredients
{
    /** @var array|Ingredient[]  */
    private $indexedIngredients;
    /** @var array|Ingredient[]  */
    private $ingredients;

    /**
     * @param Ingredient[] $ingredients
     */
    private function __construct(array $ingredients)
    {
        Assert::thatAll($ingredients)
            ->isInstanceOf(Ingredient::class);

        $this->indexedIngredients = $this->buildIndex($ingredients);
        $this->ingredients= $ingredients;
    }

    /** @return self */
    public function aggregate(): self
    {
        $ingredients = [];

        foreach ($this->indexedIngredients as $sameIngredients) {
            $ingredients[] = array_reduce($sameIngredients, function (?Ingredient $finalIngredient, Ingredient $ingredient) {
                if (null === $finalIngredient) {
                    return $ingredient;
                }

                return $finalIngredient->aggregate($ingredient);
            });
        }

        return new self($ingredients);
    }

    /**
     * @param array|Ingredient[] $ingredients
     *
     * @return array
     */
    private function buildIndex(array $ingredients): array
    {
        return array_reduce($ingredients, function (array $indexedIngredients, Ingredient $ingredient) {
            $ingredientId = (string)$ingredient->getId();
            $indexedIngredients[$ingredientId][] = $ingredient;

            return $indexedIngredients;
        }, []);
    }

    /**
     * @param self $anotherIngredientList
     *
     * @return self
     */
    public function add(self $anotherIngredientList): self
    {
        return new self(
            array_merge($this->ingredients, $anotherIngredientList->ingredients)
        );
    }

    /**
     * @param array $ingredients
     *
     * @return self
     */
    public static function fromArray(array $ingredients): self
    {
        return new self($ingredients);
    }

    /** @return array|Ingredient[] */
    public function toArray(): array
    {
        return $this->ingredients;
    }
}