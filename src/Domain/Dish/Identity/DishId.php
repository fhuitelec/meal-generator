<?php
namespace FHuitelec\MealGenerator\Domain\Dish\Identity;

use Ramsey\Uuid\UuidInterface;

class DishId
{
    /** @var UuidInterface */
    private $id;

    /**
     * @param UuidInterface $id
     */
    public function __construct(UuidInterface $id)
    {
        $this->id = $id;
    }

    /** @return string */
    public function __toString(): string
    {
        return $this->id->toString();
    }
}