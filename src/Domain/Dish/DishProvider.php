<?php
namespace FHuitelec\MealGenerator\Domain\Dish;

use FHuitelec\MealGenerator\Domain\Dish\Collection\Dishes;

interface DishProvider
{
    /** @return Dishes */
    public function findAllDishes(): Dishes;
}