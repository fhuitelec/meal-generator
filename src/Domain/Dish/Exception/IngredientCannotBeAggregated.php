<?php
namespace FHuitelec\MealGenerator\Domain\Dish\Exception;

use FHuitelec\MealGenerator\Domain\Dish\Ingredient;

class IngredientCannotBeAggregated extends \DomainException
{
    const NOT_THE_SAME_ID = 10;
    const NOT_THE_SAME_QUANTIFIER = 20;

    /** @var Ingredient */
    private $ingredient1;
    /** @var Ingredient */
    private $ingredient2;

    /**
     * @param Ingredient $ingredient1
     * @param Ingredient $ingredient2
     *
     * @return self
     */
    public static function notTheSameId(Ingredient $ingredient1, Ingredient $ingredient2): self
    {
        $exception = new self(
            'Ingredients cannot be aggregated as they don\'t have the same ID',
            self::NOT_THE_SAME_ID
        );
        $exception->setIngredient1($ingredient1);
        $exception->setIngredient2($ingredient2);

        return $exception;
    }

    /**
     * @param Ingredient $ingredient1
     * @param Ingredient $ingredient2
     *
     * @return self
     */
    public static function notTheSameQuantifier(Ingredient $ingredient1, Ingredient $ingredient2): self
    {
        $exception = new self(
            'Ingredients cannot be aggregated as they don\'t have the same Quantifier',
            self::NOT_THE_SAME_QUANTIFIER
        );
        $exception->setIngredient1($ingredient1);
        $exception->setIngredient2($ingredient2);

        return $exception;
    }

    /**
     * @param Ingredient $ingredient
     */
    public function setIngredient1(Ingredient $ingredient): void
    {
        $this->ingredient1 = $ingredient;
    }

    /**
     * @param Ingredient $ingredient
     */
    public function setIngredient2(Ingredient $ingredient): void
    {
        $this->ingredient2 = $ingredient;
    }
}