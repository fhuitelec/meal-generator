<?php
namespace FHuitelec\MealGenerator\Domain\Dish\Exception;

class NotEnoughDishesFound extends \DomainException
{
    /** @var int */
    private $expectedDishesNumber;
    /** @var int */
    private $retrievedDishesNumber;

    /**
     * @param int $expectedDishesNumber
     * @param int $retrievedDishesNumber
     *
     * @return NotEnoughDishesFound
     */
    public static function create(int $expectedDishesNumber, int $retrievedDishesNumber)
    {
        $exception = new self('Not enough dishes have been found');

        $exception->setExpectedDishesNumber($expectedDishesNumber);
        $exception->setRetrievedDishesNumber($retrievedDishesNumber);

        return $exception;
    }

    /**
     * @param int $expectedDishesNumber
     */
    public function setExpectedDishesNumber(int $expectedDishesNumber)
    {
        $this->expectedDishesNumber = $expectedDishesNumber;
    }

    /**
     * @param int $retrievedDishesNumber
     */
    public function setRetrievedDishesNumber(int $retrievedDishesNumber)
    {
        $this->retrievedDishesNumber = $retrievedDishesNumber;
    }
}