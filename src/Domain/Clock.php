<?php
namespace FHuitelec\MealGenerator\Domain;

interface Clock
{
    /** @return \DateTimeImmutable */
    public function now(): \DateTimeImmutable;
}