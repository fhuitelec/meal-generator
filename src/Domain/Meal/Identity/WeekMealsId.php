<?php
namespace FHuitelec\MealGenerator\Domain\Meal\Identity;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class WeekMealsId
{
    /** @var UuidInterface */
    private $id;

    /**
     * @param UuidInterface $id
     */
    public function __construct(UuidInterface $id)
    {
        $this->id = $id;
    }

    /** @return WeekMealsId */
    public static function random(): self
    {
        return new self(Uuid::uuid4());
    }

    /** @return string */
    public function __toString(): string
    {
        return $this->id->toString();
    }
}