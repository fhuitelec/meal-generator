<?php
namespace FHuitelec\MealGenerator\Domain\Meal;

use Assert\Assert;

class Week
{
    /** @var \DateTimeImmutable */
    private $beginning;
    /** @var \DateTimeImmutable */
    private $end;

    /**
     * @param \DateTimeImmutable $beginning
     * @param \DateTimeImmutable $end
     */
    public function __construct(\DateTimeImmutable $beginning, \DateTimeImmutable $end)
    {
        $this->beginning = $beginning;
        $this->end = $end;
    }

    /**
     * @param \DateTimeImmutable $now
     *
     * @return self
     */
    public static function startingNextMonday(\DateTimeImmutable $now): self
    {
        $monday = $now->modify('next monday');
        $sunday = $monday->modify('next sunday');

        return new self($monday, $sunday);
    }

    /**
     * @param string $day
     *
     * @return Day
     */
    public function getDay(string $day): Day
    {
        Assert::that($day)
            ->inArray(DAY::DAYS_IN_A_WEEK);

        $dayDate = $this->beginning->modify(
            sprintf('next %s', strtolower($day))
        );

        return new Day($dayDate);
    }
}