<?php
namespace FHuitelec\MealGenerator\Domain\Meal;

class Day
{
    const MONDAY = 'Monday';
    const TUESDAY = 'Tuesday';
    const WEDNESDAY = 'Wednesday';
    const THURSDAY = 'Thursday';
    const FRIDAY = 'Friday';
    const SATURDAY = 'Saturday';
    const SUNDAY = 'Sunday';

    const DAYS_IN_A_WEEK = [
        self::MONDAY,
        self::TUESDAY,
        self::WEDNESDAY,
        self::THURSDAY,
        self::FRIDAY,
        self::SATURDAY,
        self::SUNDAY
    ];

    /** @var \DateTimeImmutable */
    private $date;

    /**
     * @param \DateTimeImmutable $date
     */
    public function __construct(\DateTimeImmutable $date)
    {
        $this->date = $date;
    }

    /** @return string */
    public function getFormattedDay(): string
    {
        return ucfirst($this->date->format('l'));
    }

    /**
     * @param string $day
     *
     * @return bool
     */
    public function isDay(string $day): bool
    {
        return $this->getFormattedDay() === $day;
    }
}