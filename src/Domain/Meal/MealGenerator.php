<?php
namespace FHuitelec\MealGenerator\Domain\Meal;

use FHuitelec\MealGenerator\Domain\Clock;
use FHuitelec\MealGenerator\Domain\Dish\Collection\Dishes;
use FHuitelec\MealGenerator\Domain\Dish\Dish;
use FHuitelec\MealGenerator\Domain\Dish\DishProvider;
use FHuitelec\MealGenerator\Domain\Dish\Exception\NotEnoughDishesFound;
use FHuitelec\MealGenerator\Domain\Meal\Collection\Meals;
use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;
use FHuitelec\MealGenerator\Domain\Meal\Helper\Random;
use FHuitelec\MealGenerator\Domain\Meal\Identity\MealId;
use FHuitelec\MealGenerator\Domain\Meal\Identity\WeekMealsId;
use Ramsey\Uuid\Uuid;

class MealGenerator
{
    /** @var DishProvider */
    private $dishProvider;
    /** @var Clock */
    private $clock;

    /**
     * @param Clock        $clock
     * @param DishProvider $dishProvider
     */
    public function __construct(Clock $clock,DishProvider $dishProvider)
    {
        $this->dishProvider = $dishProvider;
        $this->clock = $clock;
    }

    /**
     * @param int $count
     *
     * @return Meal[]
     * @throws NotEnoughDishesFound
     */
    private function generate(int $count): array
    {
        $dishes = $this->getUniqueDishesThatCanBenGenerated(
            $this->dishProvider->findAllDishes()
        );

        $this->ensureExpectedDishesAreAvailable($dishes, $count);

        $randomDishes = $this->getRandomDishes($dishes, $count);

        $meals = array_map(function (Dish $dish) {
            return new Meal(
                new MealId(Uuid::fromString((string)$dish->getId())),
                $dish
            );
        }, $randomDishes->toArray());

        return $meals;
    }

    /**
     * @return WeekMeals
     *
     * @throws NotEnoughDishesFound
     */
    public function generateWeek(): WeekMeals
    {
        $meals = $this->generate(14);

        return WeekMeals::create(
            WeekMealsId::random(),
            Week::startingNextMonday($this->clock->now()),
            $meals,
            $this->clock->now()
        );
    }

    /**
     * @param Dishes $dishes
     * @param int    $count
     *
     * @return Dishes
     */
    private function getRandomDishes(Dishes $dishes, int $count): Dishes
    {
        $randomDishes = array_map(
            function (int $randomIndex) use ($dishes) {
                return $dishes->toArray()[ $randomIndex ];
            },
            Random::generateIndexes($count, $dishes->toArray())
        );

        return Dishes::fromArray($randomDishes);
    }

    /**
     * @param Dishes $dishes
     *
     * @return Dishes
     */
    private function getUniqueDishesThatCanBenGenerated(Dishes $dishes): Dishes
    {
        $dishes = array_values(array_unique($dishes->toArray(), SORT_REGULAR));

        $dishThatCanBeGenerated = array_filter($dishes, function (Dish $dish) {
            return $dish->canBeGenerated();
        });

        return Dishes::fromArray($dishThatCanBeGenerated);
    }

    /**
     * @param Dishes $dishes
     * @param int    $expectedDishesNumber
     *
     * @throws NotEnoughDishesFound
     */
    private function ensureExpectedDishesAreAvailable(Dishes $dishes, $expectedDishesNumber)
    {
        $dishRetrievedNumber = count($dishes->toArray());

        if ($dishRetrievedNumber < $expectedDishesNumber) {
            throw NotEnoughDishesFound::create($expectedDishesNumber, $dishRetrievedNumber);
        }
    }
}