<?php
namespace FHuitelec\MealGenerator\Domain\Meal\Helper;

class Random
{
    /**
     * @param int   $indexNumberToGenerate
     * @param array $involvedArray
     *
     * @return int[]
     */
    public static function generateIndexes(int $indexNumberToGenerate, array $involvedArray): array
    {
        $numbers = range(0, count($involvedArray) - 1);
        shuffle($numbers);

        return array_slice($numbers, 0, $indexNumberToGenerate);
    }
}