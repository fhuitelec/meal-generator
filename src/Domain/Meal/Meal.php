<?php
namespace FHuitelec\MealGenerator\Domain\Meal;

use FHuitelec\MealGenerator\Domain\Dish\Collection\Ingredients;
use FHuitelec\MealGenerator\Domain\Dish\Dish;
use FHuitelec\MealGenerator\Domain\Dish\Identity\DishId;
use FHuitelec\MealGenerator\Domain\Meal\Identity\MealId;

class Meal
{
    /** @var MealId */
    private $id;
    /** @var Dish */
    private $dish;

    /**
     * @param MealId $id
     * @param Dish   $dish
     */
    public function __construct(MealId $id, Dish $dish)
    {
        $this->id = $id;
        $this->dish = $dish;
    }

    /** @return MealId */
    public function getId(): MealId
    {
        return $this->id;
    }

    /** @return DishId */
    public function getDishId(): DishId
    {
        return $this->dish->getId();
    }

    /** @return string */
    public function getDishName(): string
    {
        return $this->dish->getName();
    }

    /** @return Ingredients */
    public function getDishIngredients(): Ingredients
    {
        return $this->dish->getIngredients();
    }
}