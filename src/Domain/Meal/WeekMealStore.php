<?php
namespace FHuitelec\MealGenerator\Domain\Meal;

use FHuitelec\MealGenerator\Domain\Meal\Collection\WeekMeals;

interface WeekMealStore
{
    /**
     * @param WeekMeals $weekMeals
     */
    public function store(WeekMeals $weekMeals): void;
}