<?php
namespace FHuitelec\MealGenerator\Domain\Meal\Collection;

use Assert\Assert;
use FHuitelec\MealGenerator\Domain\Dish\Collection\Ingredients;
use FHuitelec\MealGenerator\Domain\Dish\Dish;
use FHuitelec\MealGenerator\Domain\Meal\Day;
use FHuitelec\MealGenerator\Domain\Meal\Identity\MealId;
use FHuitelec\MealGenerator\Domain\Meal\Identity\WeekMealsId;
use FHuitelec\MealGenerator\Domain\Meal\Meal;
use FHuitelec\MealGenerator\Domain\Meal\Week;

class WeekMeals
{
    /** @var WeekMealsId  */
    private $id;
    /** @var Week */
    private $week;
    /** @var Meal[] */
    private $mealIdIndex;
    /** @var DayMeals[]|array */
    private $dayMeals;
    /** @var \DateTimeImmutable */
    private $createdAt;

    /**
     * @param WeekMealsId        $id
     * @param Week               $week
     * @param array              $mealIdIndex
     * @param array              $dayMeals
     * @param \DateTimeImmutable $now
     */
    private function __construct(
        WeekMealsId $id,
        Week $week,
        array $mealIdIndex,
        array $dayMeals,
        \DateTimeImmutable $now
    ) {
        $this->id = $id;
        $this->week = $week;
        $this->mealIdIndex = $mealIdIndex;
        $this->dayMeals = $dayMeals;
        $this->createdAt = $now;
    }

    /**
     * @param WeekMealsId        $id
     * @param Week               $week
     * @param array              $meals
     * @param \DateTimeImmutable $now
     *
     * @return self
     */
    public static function create(
        WeekMealsId $id,
        Week $week,
        array $meals,
        \DateTimeImmutable $now
    ): self
    {
        Assert::thatAll($meals)->isInstanceOf(Meal::class);
        Assert::that($meals)->count(14);

        $mealIdIndex = array_reduce($meals, function (array $index, Meal $meal) {
            $index[(string)$meal->getId()] = $meal;
            return $index;
        }, []);

        $dayMeals = array_map(function (string $day) use ($week, &$meals) {
            return new DayMeals(
                $week->getDay($day),
                array_pop($meals),
                array_pop($meals)
            );
        }, Day::DAYS_IN_A_WEEK);

        return new self($id, $week, $mealIdIndex, $dayMeals, $now);
    }

    /** @return WeekMealsId */
    public function getId(): WeekMealsId
    {
        return $this->id;
    }

    /**
     * @param string $day
     *
     * @return DayMeals|null
     */
    public function getByDay(string $day): ?DayMeals
    {
        foreach ($this->dayMeals as $meal) {
            if ($meal->isDay($day)) {
                return $meal;
            }
        }

        return null;
    }

    /** @return DayMeals[] */
    public function getMealsByDay(): array
    {
        return array_reduce($this->dayMeals, function (array $week, DayMeals $dayMeals) {
            $week[$dayMeals->getFormattedDay()] = $dayMeals;
            return $week;
        }, []);
    }

    /** @return \DateTimeImmutable */
    public function getCreatedAt(): \DateTimeImmutable
    {
        return $this->createdAt;
    }

    /**
     * @param MealId $mealId
     * @param Dish   $newDish
     *
     * @return self
     */
    public function replaceMeal(MealId $mealId, Dish $newDish): self
    {
        $meals = $this->mealIdIndex;

        $mealToReplace = $meals[(string)$mealId];
        $meals[(string)$mealId] = new Meal(
            $mealToReplace->getId(),
            $newDish
        );

        return self::create($this->id, $this->week, $meals, $this->createdAt);
    }

    /** @return Ingredients */
    public function getIngredients(): Ingredients
    {
        /** @var Ingredients $ingredients */
        $ingredients = array_reduce($this->dayMeals, function (Ingredients $ingredients, DayMeals $meals) {
            return $ingredients
                ->add($meals->getLunch()->getDishIngredients()
                ->add($meals->getDinner()->getDishIngredients()));
        }, Ingredients::fromArray([]));

        return $ingredients->aggregate();
    }
}