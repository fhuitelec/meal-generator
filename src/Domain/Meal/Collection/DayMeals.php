<?php
namespace FHuitelec\MealGenerator\Domain\Meal\Collection;

use FHuitelec\MealGenerator\Domain\Meal\Day;
use FHuitelec\MealGenerator\Domain\Meal\Meal;

class DayMeals
{
    /** @var Meal */
    private $lunch;
    /** @var Meal */
    private $dinner;
    /** @var Day */
    private $day;

    /**
     * @param Day  $day
     * @param Meal $lunch
     * @param Meal $dinner
     */
    public function __construct(Day $day, Meal $lunch, Meal $dinner)
    {
        $this->lunch = $lunch;
        $this->dinner = $dinner;
        $this->day = $day;
    }

    /** @return string */
    public function getFormattedDay(): string
    {
        return $this->day->getFormattedDay();
    }

    /** @return Meal */
    public function getLunch(): Meal
    {
        return $this->lunch;
    }

    /** @return Meal */
    public function getDinner(): Meal
    {
        return $this->dinner;
    }

    /**
     * @param string $day
     *
     * @return bool
     */
    public function isDay(string $day): bool
    {
        return $this->day->isDay($day);
    }

    /**
     * @return array|string[]
     */
    public function toMealNameArray(): array
    {
        return [$this->lunch->getDishName(), $this->dinner->getDishName()];
    }
}